from fabric2 import Connection
import os 
from datetime import datetime

now = datetime.now()

def deploy():
    # Activate the python virtual environment
    c.run('cd ~/httpdocs && source python-app-venv/bin/activate')

    # Install all the packages
    # requires libmysqlclient-dev, mysql-server, python3.8-dev, and build-essential in apt
    # python3.8 -m pip install mysqlclient django-simple-history==3.0.0 django-recaptcha pillow django-resized django-cors-headers django-environ
    c.run('cd ~/httpdocs/here && python3.8 -m pip install mysqlclient')
    c.run('cd ~/httpdocs/here && python3.8 -m pip install django-simple-history==3.0.0')
    c.run('cd ~/httpdocs/here && python3.8 -m pip install django-recaptcha')
    # c.run('cd ~/httpdocs/here && python3.8 -m pip install django-social-share')
    c.run('cd ~/httpdocs/here && python3.8 -m pip install pillow')
    c.run('cd ~/httpdocs/here && python3.8 -m pip install django-resized')
    c.run('cd ~/httpdocs/here && python3.8 -m pip install django-cors-headers')
    c.run('cd ~/httpdocs/here && python3.8 -m pip install django-environ')
    c.run('cd ~/httpdocs/here && python3.8 -m pip install psycopg2')
    c.run('cd ~/httpdocs/here && python3.8 -m pip install django-sendgrid-v5')

    # Update the htttp docs
    c.run('cd ~/httpdocs && git pull')

    # date = now.strftime("%Y%m%d%H%M%S")
    # launch_tag = 'l' + str(date)
    # c.run('cd ~/httpdocs && git tag ' + launch_tag)
    # c.run('cd ~/httpdocs && git push origin ' + launch_tag)

    # Update the static assets
    c.run('cd ~/httpdocs/here && python3.8 manage.py collectstatic --noinput')

    # Run the migrations
    c.run('cd ~/httpdocs/here && python3.8 manage.py migrate')

    # Touch the restart so the new code gets loaded
    c.run('cd ~/httpdocs && touch tmp/restart.txt')

c = Connection(
        user='<YOUR_USERNAME>', 
        host='<YOUR_SERVER_HOST>', 
        connect_kwargs={
            "key_filename": "/home/<YOUR_USERNAME>/.ssh/id_rsa",
    },)
result = c.run('uname -s')
result = c.run('whoami')

dir_path = os.path.dirname(os.path.realpath(__file__))
SECRET_KEY = '<YOUR_SECRET_KEY>'
path_to_manage_py = dir_path + '/inlovelikecom/'
exec_local = 'SECRET_KEY=\'' + SECRET_KEY + '\' && export SECRET_KEY && cd ' + path_to_manage_py + ' && python3.8 manage.py test -v 2'

testing_run = c.local(exec_local, shell='/bin/bash')

if testing_run.return_code == 0:
    print("DEPLOYING... tests passed.")
    deploy()
    # print("DEPLOYING")
elif testing_run.return_code == 2:
    print("Tests returned code 2, unsure how to proceed.")
else: 
    print(result)
    raise SystemExit()


# Sources: 
# https://www.programiz.com/python-programming/datetime/strftime
# https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
# https://www.digitalocean.com/community/tutorials/using-grep-regular-expressions-to-search-for-text-patterns-in-linux
# https://git-scm.com/book/en/v2/Git-Basics-Tagging