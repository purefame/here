# bash_aliases-inlovelikecom.sh
#
# (copyright) Nathaniel Steven Henry Brown
# (copyright) God
# (copyright) Jesus Heaven Christ
# (copyright) Perfection
#
# Proprietary Code
# Created: May 17, 2021
#
# Version: b.v1.0.0

### Goes in ~/.bashrc 
# if [ -f ~/.bash_aliases ]; then
#     . ~/.bash_aliases
# fi

### Goes in ~/.bash_aliases
# source ~/httpdocs/bash_aliases-hereinlovelikecom.sh

### Goes in ~/.profile
# if [ "$BASH" ]; then
#   if [ -f ~/.bashrc ]; then
#     . ~/.bashrc
#   fi
# fi

### Run this to get a secret key generated
# python3.8 -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'

here_inlovelikecom_start_server() {
	cd ~/OneDrive/code/inlovelike.com/here/here
	python3.8 manage.py runserver 0:8000
	cd ~/
}

alias here_inlovelikecom_server="here_inlovelikecom_start_server"

here_inlovelikecom_launch_code(){ 
	cd ~/OneDrive/code/inlovelike.com/here
	python3.8 deploy_fabric.py
	echo "inLoveLike.com code launched..."
	cd ~/
}

alias here_inlovelikecom_launch="here_inlovelikecom_launch_code"

here_inlovelikecom_manage_shell() {
	cd ~/OneDrive/code/inlovelike.com/here/here
	python3.8 manage.py shell
	cd ~/
}

alias here_inlovelikecom_shell="here_inlovelikecom_manage_shell"


here_inlovelikecom_start_tests() {
	cd ~/OneDrive/code/inlovelike.com/here/here
	python3.8 manage.py test -v 2
	cd ~/
}

alias here_inlovelikecom_tests="here_inlovelikecom_start_tests"


here_inlovelikecom_directory() {
	cd ~/OneDrive/code/inlovelike.com/here/here
}

alias here_inlovelikecom_dir="here_inlovelikecom_directory"


here_inlovelikecom_tmp_touch(){ 
	touch ~/OneDrive/code/inlovelike.com/here/tmp/restart.txt
}

alias here_inlovelikecom_touch="here_inlovelikecom_tmp_touch"

here_inlovelikecom_collectstatic(){ 
	cd ~/OneDrive/code/inlovelike.com/here/here
	python3.8 manage.py collectstatic --noinput
	cd ~/
}

alias here_inlovelikecom_collect="here_inlovelikecom_collectstatic"

here_inlovelikecom_change_go(){ 
	here_inlovelikecom_collect
	here_inlovelikecom_touch
	sudo /etc/init.d/apache2 restart
}

alias here_inlovelikecom_change="here_inlovelikecom_change_go"

here_inlovelikecom_tail_logs(){ 
	cd ~/OneDrive/code/inlovelike.com/logs
	tail -f django-debug.log
}

alias here_inlovelikecom_tail="here_inlovelikecom_tail_logs"

here_inlovelikecom_log(){ 
	cd ~/OneDrive/code/inlovelike.com/logs
}

alias here_inlovelikecom_log="here_inlovelikecom_log"

here_inlovelikecom_update_scripts() {
	sudo cp -R /home/nate/OneDrive/code/inlovelike.com/here/here/scripts/osprey_app-drm-inlovelikecom-v1/* /usr/local/bin/
	sudo cp -R /home/nate/OneDrive/code/inlovelike.com/here/here/scripts/osprey_app_divide_me-inlovelike-v1/* /usr/local/bin/
}

alias here_inlovelikecom_scripts="here_inlovelikecom_update_scripts"
