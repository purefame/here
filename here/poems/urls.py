# from django.urls import include, path, re_path

from . import views
from django.urls import path, re_path


app_name = 'poems'
urlpatterns = [
    path('eft_offers.html', views.eft_offers, name='eft_offers'),
    path('eft_purchases.html', views.eft_purchases, name='eft_purchases'),
    path('eft-purchase-gallery/<int:eft_purchase_id>/', views.eft_purchase_gallery, name='eft_purchase_gallery'),
    path('eft-purchase-remove/<int:eft_purchase_id>/', views.eft_purchase_remove, name='eft_purchase_remove'),
    path('eft-purchase-offer/<uuid:eft_purchase_guid>/', views.eft_purchase_offer, name='eft_purchase_offer'),
    re_path(r'^eft-purchase-offer-sold/(?P<guid>.*)/$', views.eft_purchase_offer_sold, name="eft_purchase_offer_sold"),
    re_path(r'^eft-purchase-offer-refund/(?P<guid>.*)/$', views.eft_purchase_offer_refund, name="eft_purchase_offer_refund"),
    path('eft-purchase-resell/<int:eft_purchase_id>/', views.eft_purchase_resell, name='eft_purchase_resell'),
    path('eft_purchase_upload_ajax', views.eft_purchase_upload_ajax, name='eft_purchase_upload_ajax'),
    re_path(r'^eft-transfer-get/(?P<sha512>.*).json$', views.eft_transfer_get, name="eft_transfer_get"),
]