from django import forms
from django.forms import ModelForm
# from django.core.exceptions import ValidationError
# from django.contrib.auth.models import User
# from django.utils.translation import gettext_lazy as _
# from captcha.fields import ReCaptchaField
from django.utils.html import strip_tags
from django.conf import settings
from decimal import *

class EftPurchaseUploadAjaxForm(forms.Form):
	source = forms.ChoiceField(label="Source URL", widget=forms.Select(attrs={'class': 'form-control'}), choices=settings.EFT_PURCHASE_SOURCES_CHOICES)

	sha512 = forms.CharField(label='SHA 512', widget=forms.TextInput(attrs={'class': 'form-control'}))

	key = forms.CharField(required=False, label='Transfer Key (Optional)', widget=forms.TextInput(attrs={'class': 'form-control'}))

	fields = ['source', 'sha512', 'key']

class EftPurchaseUploadCompleteAjaxForm(forms.Form):
	source = forms.ChoiceField(label="Source URL", widget=forms.Select(attrs={'class': 'form-control'}), choices=settings.EFT_PURCHASE_SOURCES_CHOICES)
	sha512 = forms.CharField(label='SHA2-512 ir SHA3-512', widget=forms.TextInput(attrs={'class': 'form-control'}))
	drm = forms.CharField(
		required=True,
		label='DRM',
		initial='', 
		widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}),
	)
	version = forms.CharField(label='Version', widget=forms.TextInput(attrs={'class': 'form-control'}))
	cost = forms.DecimalField(required=False, label='Cost', widget=forms.TextInput(attrs={'placeholder': "0.00", 'class': 'form-control'}))

	fields = ['source', 'sha512', 'drm', 'version', 'cost']

class EftPurchaseOfferForm(forms.Form):
	price = forms.DecimalField(min_value=Decimal(0.0), label='EFT Price you want to pay', widget=forms.TextInput(attrs={'placeholder': "0.00", 'class': 'form-control'}))
	name = forms.CharField(label='Your Name', widget=forms.TextInput(attrs={'class': 'form-control'}))
	email = forms.EmailField(label='Your Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
	comment = forms.CharField(
		required=False,
		label='Any comments?',
		initial='', 
		widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}),
	)

	if settings.FEATURE_CAPTCHA:
		captcha = ReCaptchaField()

	fields = ['price', 'name', 'email', 'comment']


# Sources: 
# https://docs.djangoproject.com/en/3.1/topics/forms/modelforms/
# https://docs.djangoproject.com/en/3.1/ref/forms/api/
# https://docs.djangoproject.com/en/3.1/ref/forms/fields/
# https://docs.djangoproject.com/en/3.1/ref/models/fields/
# https://docs.djangoproject.com/en/3.1/ref/forms/validation/
# https://stackoverflow.com/questions/48030567/how-to-customize-username-validation
# https://stackoverflow.com/questions/9324432/how-to-create-password-input-field-in-django
# https://stackoverflow.com/questions/11443658/how-do-i-add-a-class-to-a-choicefield-in-django/11443704
# https://stackoverflow.com/questions/6862250/change-a-django-form-field-to-a-hidden-field
# https://docs.djangoproject.com/en/3.2/ref/forms/fields/