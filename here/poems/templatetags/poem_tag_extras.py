from django import template
import re
import urllib
from datetime import datetime
import pytz
from django.conf import settings
from django.utils.html import escape, strip_tags
from members.templatetags import member_tag_extras
from pages.templatetags import page_tag_extras
from urllib.parse import unquote 
import json
import base64
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

register = template.Library()


@register.filter
def host(request):
	return settings.HTTPS_BASE_URL

@register.filter
def host_no_slash(request):
	return host(False)[:-1]

@register.filter
def mod_min(i):
	# Last time the site was manually updated across the board
	# Such occurances are like when the title has changed
	if i:
		return i
	else:
		utc=pytz.UTC
		mm = datetime.strptime(settings.MOD_MIN_STR_DATE_YYYYMMDDHHMM, '%Y/%m/%d %H:%M')
		return utc.localize(mm)

@register.filter(takes_context=True)
def active_link_class(request, url):
	if unquote(request.path) == unquote(url):
		return "active"		
	else:
		return ""

@register.filter(takes_context=True)
def active_link_class_profile(request):
	if unquote(request.path) == unquote(member_tag_extras.author_url(request.user)):
		return "active"	
	else:
		return ""

@register.filter
def eft_cost_per_byte(poem):
	return '<i class="fab fa-btc"></i> ' + str(settings.EFT_COST_PER_BYTE).strip()

@register.filter
def eft_purchase_is_resold(eft_purchase):
	if eft_purchase is None:
		return False

	offers = eft_purchase.eft_purchase_offer_eft_purchase.all()

	sold = False
	for o in offers:
		if o.sold:
			sold = o.sold_on

	return sold


@register.filter
def eft_purchase_eft_sha512(eft_purchase):
	if eft_purchase is None:
		return False

	eft_purchase = eft_purchase.eft_resell_eft_purchase.first()

	if eft_purchase is not None:
		return eft_purchase.sha512
	else:
		return False


@register.filter
def eft_purchase_is_sold(eft_purchase):
	if eft_purchase is None:
		return False

	offers = eft_purchase.eft_purchase_offer_eft_purchase.all()

	sold = False
	for o in offers:
		if o.sold:
			return True

	return sold


@register.filter
def eft_purchase_resell_cost(eft_purchase):
	if eft_purchase is None:
		return False

	eft_resell = eft_purchase.eft_resell_eft_purchase.first()

	if eft_resell is not None:
		if eft_resell.cost:
			return '<i class="fab fa-btc"></i> ' + str(float(eft_resell.cost)).strip()
		else: 
			return False
	else:
		return False


@register.filter
def eft_purchase_author_guid(eft_purchase):
	if eft_purchase is None:
		return False

	if eft_purchase.version == '1.1.0':
		base64_decoded = base64.b64decode(eft_purchase.base64).decode("utf-8") 
		eft_purchase_json = json.loads(base64_decoded)
		return eft_purchase_json['submitter']['guid']

	return False


@register.filter
def eft_purchase_author_display(eft_purchase):
	if eft_purchase is None:
		return False

	if eft_purchase.version == '1.0.0':
		eft_purchase_json = json.loads(eft_purchase.eft_file)
		if eft_purchase_json['poem']['submitter']['first_name'] or eft_purchase_json['poem']['submitter']['last_name']:
			name = " ".join([eft_purchase_json['poem']['submitter']['first_name'], eft_purchase_json['poem']['submitter']['last_name']])
			return name
		elif eft_purchase_json['poem']['submitter']['username']:
			return eft_purchase_json['poem']['submitter']['username']
		else:
			return eft_purchase_json['poem']['submitter']['email']
	elif eft_purchase.version == '1.1.0':
		base64_decoded = base64.b64decode(eft_purchase.base64).decode("utf-8") 
		eft_purchase_json = json.loads(base64_decoded)

		if eft_purchase_json['submitter']['first_name'] or eft_purchase_json['submitter']['last_name']:
			name = " ".join([eft_purchase_json['submitter']['first_name'], eft_purchase_json['submitter']['last_name']])
			return name
		elif eft_purchase_json['submitter']['username']:
			return eft_purchase_json['submitter']['username']
		else:
			return eft_purchase_json['submitter']['email']


	return 'Unknown'


@register.filter
def poem_is_code(poem_title):
	if re.findall(r"\<CODE\>", poem_title, flags=re.IGNORECASE):
		return True

	return False


@register.filter
def poem_as_display_code(poem_body):
	display = ''
	poem_body_lines = poem_body.splitlines()

	if len(poem_body_lines) > 21:
		max_lines_range = range(21)
		cut_off = True
	else:
		max_lines_range = range(len(poem_body_lines))
		cut_off = False

	for x in max_lines_range:
		display = display + poem_body_lines[x] + "\r\n"

	if cut_off:
		display = display + "[...]"

	return display


@register.filter
def eft_purchase_offer_destination(offer):
	if offer is None:
		return False

	eft_purchase = offer.eft_purchase.eft_resell_eft_purchase.first()

	if eft_purchase.destination:
		destination = eft_purchase.destination
	else:
		destination = False

	return destination

@register.filter
def eft_purchase_eft_sha512(eft_purchase):
	if eft_purchase is None:
		return False

	eft_purchase = eft_purchase.eft_resell_eft_purchase.first()

	if eft_purchase is not None:
		return eft_purchase.sha512
	else:
		return False


@register.filter
def eft_purchase_is_sold(eft_purchase):
	if eft_purchase is None:
		return False

	offers = eft_purchase.eft_purchase_offer_eft_purchase.all()

	sold = False
	for o in offers:
		if o.sold:
			return True

	return sold

@register.filter
def eft_purchase_file_key(eft_purchase):
	if eft_purchase is None:
		return False

	return eft_purchase.file().key

@register.filter
def q_request_get(request):
	q_get = request.GET.get('q', False)
	if q_get:
		q_get = re.sub(r"[^0-9a-zA-Z\'\";\-,\.\s]+", "", q_get)
	return q_get


@register.filter
def search_q_value(request):
	return request.GET.get('q', '')
	
@register.filter
def eft_purchase_history(eft_purchase):
	if eft_purchase is None:
		return 'No history to display'

	html = ''
	if eft_purchase.version == '1.1.0':
		base64_decoded = base64.b64decode(eft_purchase.file().base64).decode("utf-8") 
		eft_purchase_json = json.loads(base64_decoded)

		for history in eft_purchase_json['source_history']:
			# 2021-04-27T19:02:55.567Z
			ymd = re.match(r'(\d+)-(\d+)-(\d+)T.*', history['created_on'])

			datetime_object = datetime.strptime(ymd[1] + '-' + ymd[2] + '-' + ymd[3], '%Y-%m-%d')

			html = html + '<span class="text-info">' + datetime_object.strftime('%B %d, %Y') + '</span> '
			# 
			html = html + '<span class="btn btn-dark text-light">' + page_tag_extras.cost_as_display(history['cost']) + '</span>  '
			html = html + '<a href="' + history['source'] + 'eft-' + history['sha512'] + '.json" class="btn btn-sm btn-light ml-2" data-toggle="tooltip" data-placement="bottom" title="Click for the EFT File. It contains all the relevent data associated to this historical poem record."><i class="fas fa-coins"></i></a> '
			html = html + '<span class="text-success">' + history['sha512'] + '</span><br /><br />'

	if len(html) == 0:
		html = 'No history to display'

	return html


@register.filter
def poem_is_free(poem_title):
	if re.findall(r"\<FREE\>", poem_title, flags=re.IGNORECASE):
		return True

	return False

@register.filter
def poem_title_short(l):
	if not l:
		return False
	max = settings.POEM_TITLE_SHORT_LENGTH
	if len(l) > max:
		return strip_tags(l)[:max] + "..."
	else:
		return strip_tags(l)


@register.filter
def poem_is_file(poem_title):
	if not poem_title:
		return False

	if re.findall(r"\<FILE\>", poem_title, flags=re.IGNORECASE):
		return True

	return False


@register.filter
def poem_file_cache_is_image(poem_file):
	if re.findall(r"\.GIF", str(poem_file), flags=re.IGNORECASE):
		return True
	if re.findall(r"\.PNG", str(poem_file), flags=re.IGNORECASE):
		return True
	if re.findall(r"\.JPG", str(poem_file), flags=re.IGNORECASE):
		return True
	if re.findall(r"\.JPEG", str(poem_file), flags=re.IGNORECASE):
		return True

	return False

@register.filter
def poem_file_cache_is_audio(poem_file):
	if re.findall(r"\.M4A", str(poem_file), flags=re.IGNORECASE):
		return True

	return False

@register.filter
def poem_file_cache_is_video(poem_file):
	if re.findall(r"\.MP4", str(poem_file), flags=re.IGNORECASE):
		return True

	return False


@register.filter
def poem_is_code_full(poem_title):
	if not poem_title:
		return False

	if re.findall(r"\<CODE\>", poem_title, flags=re.IGNORECASE):
		if re.findall(r"\<FULL\>", poem_title, flags=re.IGNORECASE):
			return True

	return False

@register.filter
def poem_as_display_code_full(poem_body):
	if not poem_body:
		return ''

	display = '<code>'
	poem_body_lines = poem_body.splitlines()

	max_lines_range = range(len(poem_body_lines))

	for x in max_lines_range:
		display = display + poem_body_lines[x] + "\r\n"

	return display + '</code>'
