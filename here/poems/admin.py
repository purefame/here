from django.contrib import admin
from .models import EftPurchase, EftPurchaseOffer, EftResell

# Register your models here.

admin.site.register([EftPurchase, EftPurchaseOffer, EftResell]) 