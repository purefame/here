from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import redirect
from django.utils import timezone
from django.db.models import Q, OuterRef
from django.core.serializers.json import DjangoJSONEncoder

from poems.models import EftPurchase, EftPurchaseOffer, EftResell, DivideMe
from poems.forms import EftPurchaseUploadAjaxForm, EftPurchaseUploadCompleteAjaxForm, EftPurchaseOfferForm
from members.templatetags import member_tag_extras
from members.models import Member
import logging
import re
import functools
import operator
import hashlib
import json
from decimal import *

# Get an instance of a logger
logger = logging.getLogger(__name__)


# Create your views here.

@csrf_exempt
def eft_get(request, sha512):
	eft = False
	json_data = {
		'error': True,
		'message': 'Default',
	}

	divide_me = DivideMe()
	source_contract_address = divide_me.get_contract_address_for_host(settings.HTTPS_BASE_URL)

	try:
		eft_resell = EftResell.objects.get(sha512=sha512)
	except EftResell.DoesNotExist:
		eft_resell = False
		json_data['message'] = 'EFT Does not exist'
	except Exception as e:
		json_data = { 'success': False, 'message': 'Exception occurred. ' + str(e)}
		eft_resell = False

	if eft_resell:
		json_data = {
			'cost': eft_resell.cost,
			'drm': eft_resell.drm,
			'sha512': eft_resell.sha512,
			'sha3_512': eft_resell.sha3_512,
			'version': settings.EFT_CURRENT_VERSION,
			'source': settings.HTTPS_BASE_URL,
		}
		json_data['member_contract_address'] = eft_resell.eft_purchase.member().contract_address

	json_data['source_contract_address'] = source_contract_address

	response = JsonResponse(json_data)
	return response

@login_required
def eft_offers(request):
	member = request.user.member_user.first()

	eft_purchases = EftPurchase.objects.filter(owner=request.user).values_list('id', flat=True)
	resell_offers = EftPurchaseOffer.objects.filter(eft_purchase__in=list(eft_purchases)).order_by('created').reverse()

	resell_offers

	options = {
		'resell_offers': resell_offers,
		'success': request.GET.get('success', False),
		'error': request.GET.get('error', False),
		'seo_title': 'EFT Requests with inLoveLike'
	}
	return render(request, 'poems/eft_offers.html', options)


@login_required
def eft_purchases(request):
	user = request.user
	member = user.member_user.first()

	if member.standing == settings.MEMBER_STANDING_CANCELLED:
		response = redirect('/members/intro.html?error=cancelled')
		return response

	eft_purchases = EftPurchase.objects.filter(owner=user).reverse()

	form_eft_ajax = EftPurchaseUploadAjaxForm()

	options = {
		'eft_purchases': eft_purchases,
		# 'form': form,
		'form_eft_ajax': form_eft_ajax,
		'success': request.GET.get('success', False),
		'error': request.GET.get('error', False),
		'message': request.GET.get('message', False),
	}
	return render(request, 'poems/eft_purchases.html', options)


@login_required
def eft_purchase_upload_ajax(request):
	user = request.user
	member = user.member_user.first()

	json_data = { 'success': False }

	if member.standing != settings.MEMBER_STANDING_CANCELLED:
		if request.method == 'POST':
			valid = False

			form = EftPurchaseUploadCompleteAjaxForm(request.POST or None)
			if form.is_valid():
				# Verify valid data from the url
				# {{ source }}poems/eft-verify/{{ sha512 }}.json
				sha512 = form.cleaned_data["sha512"]
				source = form.cleaned_data["source"]
				drm = form.cleaned_data["drm"]
				version = form.cleaned_data["version"]
				cost = form.cleaned_data["cost"]

				print("eft_purchase_load_ajax-> success is True")

				# If valid, get the base64 and version
				# {{ source }}poems/eft-get/{{ sha512 }}.json

				eft_file_data = {
					'sha512': sha512,
					'source': source,
					'version': version,
					'drm': drm,
					'cost': cost,
				}

				eft_file_str = json.dumps(eft_file_data, cls=DjangoJSONEncoder, default = str)

				try:
					eft_purchase = EftPurchase()
					eft_purchase.eft_file = eft_file_str
					eft_purchase.owner = request.user
					eft_purchase.save()
					json_data = { 'success': True }
				except Exception as e:
					json_data = { 'success': False, 'message': 'An exception has occurred: ' + str(e) }
					if form.errors:
						json_data['message'] = json_data['message'] + form.errors.join(", ")
			else:
				json_data = { 'success': False, 'message': 'Form is not valid', 'form': form.errors }

	response = JsonResponse(json_data)
	return response

@login_required
def eft_purchase_gallery(request, eft_purchase_id):
	eft_purchase = get_object_or_404(EftPurchase, pk=eft_purchase_id)

	member = request.user.member_user.first()
	if eft_purchase.owner != request.user and request.user.is_staff is False:
		response = redirect('/poems/eft_purchases.html?error=eft_purchase_gallery')
		return response

	if eft_purchase.visible is True:
		eft_purchase.visible = False
	else:
		eft_purchase.visible = True

	eft_purchase.save()

	response = redirect('/poems/eft_purchases.html?success=eft_purchase_gallery')
	return response


@login_required
def eft_purchase_remove(request, eft_purchase_id):
	eft_purchase = get_object_or_404(EftPurchase, pk=eft_purchase_id)

	member = request.user.member_user.first()
	if eft_purchase.owner != request.user and request.user.is_staff is False:
		response = redirect('/poems/eft_purchases.html?error=eft_purchase_remove')
		return response

	if request.user.is_staff is False:
		response = redirect('/poems/eft_purchases.html?error=not_admin')
		return response

	eft_purchase.delete()

	response = redirect('/poems/eft_purchases.html?success=eft_purchase_remove')
	return response


def eft_purchase_offer(request, eft_purchase_guid):
	eft_purchase = get_object_or_404(EftPurchase, guid=eft_purchase_guid)

	request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

	eft_resell = eft_purchase.file()

	if request.user.is_authenticated:
		price = request.POST.get('price', eft_resell.cost)
		name = request.POST.get('name', member_tag_extras.author_title(request.user))
		email = request.POST.get('email', request.user.email)
		comment = request.POST.get('comment', '')
	else:
		price = request.POST.get('price', eft_resell.cost)
		name = request.POST.get('name', '')
		email = request.POST.get('email', '')
		comment = request.POST.get('comment', '')

	eft_purchase_offer_form_data = {
		'price': price,
		'name' : name,
		'email' : email,
		'comment' : comment,
	}

	if request.method == 'POST':
		# I Wish I could have more than one Form on the POST so variables don't conflict
		# request.POST['Poem'].get('title', 'Empty')
		# request.POST['Member'].get('pricing', settings.MEMBER_PRICING_FREE)
		form = EftPurchaseOfferForm(eft_purchase_offer_form_data)
		if form.is_valid():
			try:
				if form.cleaned_data['price']:
					price = form.cleaned_data['price']
				else:
					price = Decimal(0)
					
				epo = EftPurchaseOffer.objects.create(
					price=price,
					name=form.cleaned_data['name'],
					email=form.cleaned_data['email'],
					comment=form.cleaned_data['comment'],
					eft_purchase=eft_purchase,
				)
				response = redirect('/poems/eft-purchase-offer/' + str(eft_purchase.guid) + '/?success=created&po=' + str(epo.price))
				return response
			except Exception as e:
				logger.info("eft_purchase_offer -> Exception")
				logger.info(e)
				response = redirect('/poems/eft-purchase-offer/' + str(eft_purchase.guid) + '/?error=created&e=' + str(e))
				return response

	else:
		form = EftPurchaseOfferForm(eft_purchase_offer_form_data)

	options = {
		'seo_title': 'Purchase a poem as an EFT with inLoveLike',
		'eft_purchase': eft_purchase,
		'form': form,
		'eft_resell': eft_resell,
		'success': request.GET.get('success', False),
		'error': request.GET.get('error', False),
	}
	return render(request, 'poems/eft_purchase_offer.html', options)

@login_required
def eft_purchase_offer_sold(request, guid):
	eft_purchase_offer = get_object_or_404(EftPurchaseOffer, pk=guid)

	member = request.user.member_user.first()
	if eft_purchase_offer.eft_purchase.owner != request.user and request.user.is_staff is False:
		response = redirect('/poems/eft_offers.html?error=resold_not_owner')
		return response

	if eft_purchase_offer.sold is False:

		# subject, from_email, to = 'hello', 'nate@purefame.com', 'nate@purefame.com'
		# text_content = 'This is an important message.'
		# html_content = '<p>This is an <strong>important</strong> message.</p>'
		# msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
		# msg.attach_alternative(html_content, "text/html")
		# msg.send()

		now = timezone.now()
		eft_purchase_offer.sold = True
		eft_purchase_offer.sold_on = now
		eft_purchase_offer.key = hashlib.sha512(str(timezone.now()).encode('utf-8')).hexdigest()
		eft_purchase_offer.save()

		# subject = 'You have acquired an Electronic File Token (EFT) with inLoveLike'
		# html_message = render_to_string('poems/eft_sold_email.html', {'eft': eft})
		# plain_message = strip_tags(html_message)
		# from_email = 'inLoveLike <' + settings.EMAIL_FROM + '>'
		# to = eft.email

		# mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
		# mail.send_mail('Subject here', 'Here is the message.', 'nate@purefame.com', ['nate@purefame.com'], fail_silently=False)
		response = redirect('/poems/eft_offers.html?success=resold')
		return response
	else:
		response = redirect('/poems/eft_offers.html?error=resold')
		return response


@login_required
def eft_purchase_offer_refund(request, guid):
	eft_purchase_offer = get_object_or_404(EftPurchaseOffer, pk=guid)

	member = request.user.member_user.first()
	if eft_purchase_offer.eft_purchase.owner != request.user and request.user.is_staff is False:
		response = redirect('/poems/eft_offers.html?error=refund_not_owner')
		return response

	if request.user.is_staff is False:
		response = redirect('/poems/eft_offers.html?error=not_admin')
		return response

	if eft_purchase_offer.sold:
		eft_purchase_offer.sold = False
		eft_purchase_offer.sold_on = None
		eft_purchase_offer.destination = None
		eft_purchase_offer.key = None
		eft_purchase_offer.save()
		response = redirect('/poems/eft_offers.html?success=resold_refund')
		return response
	else:
		response = redirect('/poems/eft_offers.html?error=resold_refund')
		return response

@login_required
def eft_purchase_resell(request, eft_purchase_id):
	eft_purchase = get_object_or_404(EftPurchase, pk=eft_purchase_id)

	member = request.user.member_user.first()
	if eft_purchase.owner != request.user and request.user.is_staff is False:
		response = redirect('/poems/eft_purchases.html?error=eft_purchase_resell')
		return response

	if eft_purchase.resell_exclusion is True:
		eft_purchase.resell_exclusion = False
	else:
		eft_purchase.resell_exclusion = True

	eft_purchase.save()

	response = redirect('/poems/eft_purchases.html?success=eft_purchase_resell')
	return response


@csrf_exempt
def eft_transfer_get(request, sha512):
	json_data = {
		'success': False,
		'message': 'Default',
	}

	if request.method == 'POST':
		transfer_key = request.POST.get('key', False)

		try:
			eft_resell = EftResell.objects.get(sha512=sha512)
		except EftResell.DoesNotExist:
			eft_resell = False
		except Exception as e:
			eft_resell = False

		if not eft_resell:
			try:
				eft_resell = EftResell.objects.get(sha3_512=sha512)
			except EftResell.DoesNotExist:
				eft_resell = False
				json_data['message'] = 'EFT SHA512 not found'
			except Exception as e:
				eft_resell = False
				json_data['message'] = 'An exception has occurred. ' + str(e)

		if eft_resell:
			if eft_resell.is_free():
				dest = request.POST.get("destination", False)
				if dest:
					eft_resell.destination = 'Freely distributed: Last destination: ' + dest + ', last imported on: ' + str(timezone.now())
				else:
					eft_resell.destination = 'Freely distributed. Last imported on: ' + str(timezone.now())

				eft_resell.save()

				json_data = {
					'success': True,
					'drm': eft_resell.drm,
					'cost': eft_resell.cost,
					'sha512': eft_resell.sha512,
					'sha3_512': eft_resell.sha3_512,
					'version': settings.EFT_CURRENT_VERSION,
					'source': settings.HTTPS_BASE_URL,
				}
			else:
				try:
					offer_by_key = eft_resell.eft_purchase.eft_purchase_offer_eft_purchase.filter(key=transfer_key).first()
				except Exception as e:
					offer_by_key = False

				if offer_by_key:
					if offer_by_key.sold:
						dest = request.POST.get("destination", False)
						if offer_by_key.destination and len(offer_by_key.destination) > 0 and offer_by_key.destination != dest:
							json_data['message'] = 'Destination already set on EFT offer'
						else:
							offer_by_key.destination = dest
							offer_by_key.save()

							json_data = {
								'success': True,
								'drm': eft_resell.drm,
								'cost': eft_resell.cost,
								'sha512': eft_resell.sha512,
								'sha3_512': eft_resell.sha3_512,
								'version': settings.EFT_CURRENT_VERSION,
								'source': settings.HTTPS_BASE_URL,
							}
					else:
						json_data['message'] = 'EFT offer has not been marked as sold'
				else:
					json_data['message'] = 'EFT offer not found by transfer key.'

	response = JsonResponse(json_data)
	return response


def search_get(request):
	request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

	term = request.GET.get('q', '')
	return search(request, term)


def search_advanced(request):
	request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

	case_sensitive_get = request.GET.get('case_sensitive', False)
	case_sensitive_get = True if case_sensitive_get == 'True' else False
	q_get = request.GET.get('q', '')

	if q_get:
		users_not_cancelled = Member.objects.exclude(standing=settings.MEMBER_STANDING_CANCELLED).values_list('user_id', flat=True)
		eft_purchase_matches = EftPurchase.objects.filter(owner__in=list(users_not_cancelled))

		if q_get:
			q_get = re.sub(r"[^0-9a-zA-Z\'\";\-,\.\s]+", "", q_get)
			terms = q_get.split(" ")
			search_terms = []
			if case_sensitive_get:
				search_terms.append(Q(poem_title__contains=' '.join(terms)))
				search_terms.append(Q(poem_body__contains=' '.join(terms)))
			else:
				search_terms.append(Q(poem_title__icontains=' '.join(terms)))
				search_terms.append(Q(poem_body__icontains=' '.join(terms)))

			eft_purchase_matches = eft_purchase_matches.filter(functools.reduce(operator.or_, search_terms))

		eft_purchase_matches = eft_purchase_matches.exclude(visible=False).order_by('imported_on').distinct().reverse()
	else:
		eft_purchase_matches = []

	options = {
		'seo_title': 'Poetry advanced search - inLoveLike - Poetry with Poems',
		'seo_description': 'Filter all Poems with advanced search functionality.',
		# 'term': term,
		'eft_purchase_matches': eft_purchase_matches,
		'q_get': q_get,
		'case_sensitive_get': case_sensitive_get,
	}
	return render(request, 'poems/search_advanced.html', options)

def search(request, term):
	request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

	term = re.sub(r"[^0-9a-zA-Z\'\";\-,\.\s]+", "", term.replace('+', ' '))
	terms = term.split(" ")
	search_terms = []
	# mysql 8.0 feature
	# search_terms.append(Q(title__regex='[-,;_\s]?'.join(terms)))
	# search_terms.append(Q(body__regex='[-,;_\s]?'.join(terms)))
	search_terms.append(Q(poem_title__icontains=' '.join(terms)))
	search_terms.append(Q(poem_body__icontains=' '.join(terms)))

	members_not_cancelled = Member.objects.exclude(standing=settings.MEMBER_STANDING_CANCELLED).values_list('user_id', flat=True)
	eft_purchase_matches = EftPurchase.objects.filter(owner__in=list(members_not_cancelled)).exclude(visible=False).filter(functools.reduce(operator.or_, search_terms)).distinct()

	options = {
		'seo_title': 'EFT Poetry search results for "' + term + '" - inLoveLike - Poetry with Poems',
		'seo_description': 'Showing ' + str(len(eft_purchase_matches)) + ' results for EFT Poetry search "' + term + '..."',
		'term': term,
		'q_get' : term,
		'eft_purchase_matches': eft_purchase_matches,
	}
	return render(request, 'poems/search.html', options)
