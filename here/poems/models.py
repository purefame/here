from django.utils import timezone
from django.utils.timezone import now
from django.db import models
from django.contrib.auth.models import User
from django.utils.html import strip_tags
from django.http import JsonResponse
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings
from simple_history.models import HistoricalRecords
from pages.templatetags import page_tag_extras
from poems.templatetags import poem_tag_extras
import re
import uuid
import json
import hashlib
import subprocess
import base64
import urllib.request
import os.path as os_path
import os
from io import StringIO
from uuid import UUID
from decimal import *


class EftFileParser:
	eft_file = ''
	drm = ''
	base64 = ''
	base64_decoded = ''
	base64_plain = ''
	base64_json = ''
	sha512 = ''
	sha3_512 = ''
	version = ''
	supported_versions = settings.EFT_SUPPORTED_VERSIONS
	valid_sources = settings.EFT_PURCHASE_VALID_SOURCES
	white_list_sources = settings.EFT_PURCHASE_WHITE_LIST_SOURCES
	eft_file_as_json = ''
	poem_guid = ''
	poem_title = ''
	poem_body = ''
	source = ''
	cost = None
	parsed = False

	def __init__(self, eft_file, db_collation=None):
		self.eft_file = eft_file
		self.eft_file_json = json.loads(self.eft_file, strict=False)
		self.version = self.eft_file_json['version']

		if self.is_valid_version() and self.is_verified():
			if self.version == '1.0.0':
				self.poem_guid = self.eft_file_json['poem']['guid']
				self.poem_title = self.eft_file_json['poem']['title']
				self.poem_body = self.eft_file_json['poem']['body']
				self.parsed = True
			elif self.version == '1.1.0':
				self.source = self.eft_file_json['source']
				# Verify hash of poem in eft json object
				# print(self.eft_file_json['base64'])
				verify_poem_sha512 = hashlib.sha512(self.eft_file_json['base64'].encode('utf-8')).hexdigest()
				
				if verify_poem_sha512 == self.eft_file_json['sha512']:
					self.cost = self.eft_file_json['cost']
					self.base64 = self.eft_file_json['base64']
					self.sha512 = self.eft_file_json['sha512']
					# Package has been verified
					self.base64_decoded = base64.b64decode(self.eft_file_json['base64']).decode("utf-8") 
					self.base64_json = json.loads(self.base64_decoded)
				# Uncompress poem in eft json object
				# Assign object values from uncompressed object

					self.poem_guid = self.base64_json['guid']
					self.poem_title = self.base64_json['title']
					self.poem_body = self.base64_json['body']

					self.file_tmp_path = self.__decode_poem_body_as_file()

					self.parsed = True
				else:
					print("EftFile -> hash check failed")
			elif self.version == '1.2.0':
				self.source = self.eft_file_json['source']
				# Verify hash of poem in eft json object
				# print(self.eft_file_json['base64'])
				verify_poem_sha512 = hashlib.sha512(self.eft_file_json['base64'].encode('utf-8')).hexdigest()
				verify_poem_sha3_512 = hashlib.sha3_512(self.eft_file_json['base64'].encode('utf-8')).hexdigest()

				verified = False
				if self.eft_file_json.get('sha512') and verify_poem_sha512 == self.eft_file_json['sha512']:
					verified = True
				elif self.eft_file_json.get('sha3_512') and verify_poem_sha512 == self.eft_file_json['sha3_512']:
					verified = True
				elif self.eft_file_json.get('sha512') and verify_poem_sha3_512 == self.eft_file_json['sha512']:
					verified = True
				elif self.eft_file_json.get('sha3_512') and verify_poem_sha3_512 == self.eft_file_json['sha3_512']:
					verified = True

				if verified:
					self.cost = self.eft_file_json['cost']
					self.base64 = self.eft_file_json['base64']
					self.sha512 = verify_poem_sha512
					self.sha3_512 = verify_poem_sha3_512
					# Package has been verified
					self.base64_decoded = base64.b64decode(self.eft_file_json['base64']).decode("utf-8") 
					self.base64_json = json.loads(self.base64_decoded)
				# Uncompress poem in eft json object
				# Assign object values from uncompressed object

					self.poem_guid = self.base64_json['guid']
					self.poem_title = self.base64_json['title']
					self.poem_body = self.base64_json['body']

					self.file_tmp_path = self.__decode_poem_body_as_file()

					self.parsed = True
				else:
					print("EftFile -> hash check failed")
			elif self.version == '2.0.0':
				self.source = self.eft_file_json['source']
				# Verify hash of poem in eft json object
				# print(self.eft_file_json['base64'])
				verify_poem_sha512 = hashlib.sha512(self.eft_file_json['base64'].encode('utf-8')).hexdigest()
				verify_poem_sha3_512 = hashlib.sha3_512(self.eft_file_json['base64'].encode('utf-8')).hexdigest()

				print('EftFileParser.init -> verify_poem_sha512: ' + verify_poem_sha512)
				print('EftFileParser.init -> verify_poem_sha3_512: ' + verify_poem_sha3_512)
				print('EftFileParser.init -> eft_file_json[sha512]: ' + str(self.eft_file_json.get('sha512')))
				print('EftFileParser.init -> eft_file_json[sha3_512]: ' + str(self.eft_file_json.get('sha3_512')))

				verified = False
				if self.eft_file_json.get('sha512') and verify_poem_sha512 == self.eft_file_json.get('sha512'):
					verified = True
				elif self.eft_file_json.get('sha3_512') and verify_poem_sha512 == self.eft_file_json.get('sha3_512'):
					verified = True
				elif self.eft_file_json.get('sha512') and verify_poem_sha3_512 == self.eft_file_json.get('sha512'):
					verified = True
				elif self.eft_file_json.get('sha3_512') and verify_poem_sha3_512 == self.eft_file_json.get('sha3_512'):
					verified = True

				if verified:
					self.cost = self.eft_file_json['cost']
					self.base64 = self.eft_file_json['base64']
					self.sha512 = verify_poem_sha512
					self.sha3_512 = verify_poem_sha3_512
					# Decrypt the base64 and use values

					poem_body_plain_file_uuid = uuid.uuid4()

					base64_plain_path = settings.MEDIA_ROOT + 'tmp/EftFileParser-whole-plain-' + str(poem_body_plain_file_uuid)
					base64_encrypted_path = settings.MEDIA_ROOT + 'tmp/EftFileParser-whole-encrypted-' + str(poem_body_plain_file_uuid)
					
					with open(base64_encrypted_path, 'w') as f_b64_p_p:
						f_b64_p_p.write(self.eft_file_json['base64'])
						f_b64_p_p.close()

					if os_path.isfile(base64_encrypted_path) and not os_path.isfile(base64_plain_path): 
						cmd = 'osprey_app-drm-inlovelikecom-v1 ' + self.source + ' "' + base64_encrypted_path + '" reverse > "' + base64_plain_path + '"'
						print('EftFileParser.init -> running command: ' + cmd)
						os.system(cmd)
					else:
						print('EftFileParser.init-> base64_plain_path file missing or base64_encrypted_path exists')

					base64_return = ''

					if os_path.isfile(base64_plain_path): 
						with open(base64_plain_path, 'r') as f_b64_e_p:
							self.base64_plain = f_b64_e_p.read().encode('utf-8').strip()
							f_b64_e_p.close()

					self.base64_decoded = base64.b64decode(self.base64_plain.decode('utf-8')).decode("utf-8", "ignore") 
					self.base64_json = json.loads(self.base64_decoded)
				# Uncompress poem in eft json object
				# Assign object values from uncompressed object

					self.poem_guid = self.base64_json['guid']
					self.poem_title = self.base64_json['title']
					self.poem_body = self.base64_json['body']

					self.file_tmp_path = self.__decode_poem_body_as_file()

					self.parsed = True
				else:
					print("EftFileParser -> hash check failed")
			elif self.version == '3.0.0':
				self.source = self.eft_file_json['source']
				# Verify hash of poem in eft json object
				# print(self.eft_file_json['base64'])
				verify_poem_sha512 = hashlib.sha512(self.eft_file_json['drm'].encode('utf-8')).hexdigest()
				verify_poem_sha3_512 = hashlib.sha3_512(self.eft_file_json['drm'].encode('utf-8')).hexdigest()

				print('EftFileParser.init -> verify_poem_sha512: ' + verify_poem_sha512)
				print('EftFileParser.init -> verify_poem_sha3_512: ' + verify_poem_sha3_512)
				print('EftFileParser.init -> eft_file_json[sha512]: ' + str(self.eft_file_json.get('sha512')))
				print('EftFileParser.init -> eft_file_json[sha3_512]: ' + str(self.eft_file_json.get('sha3_512')))

				verified = False
				if self.eft_file_json.get('sha512') and verify_poem_sha512 == self.eft_file_json.get('sha512'):
					verified = True
				elif self.eft_file_json.get('sha3_512') and verify_poem_sha512 == self.eft_file_json.get('sha3_512'):
					verified = True
				elif self.eft_file_json.get('sha512') and verify_poem_sha3_512 == self.eft_file_json.get('sha512'):
					verified = True
				elif self.eft_file_json.get('sha3_512') and verify_poem_sha3_512 == self.eft_file_json.get('sha3_512'):
					verified = True

				if verified:
					self.cost = self.eft_file_json['cost']
					self.drm = self.eft_file_json['drm']
					self.sha512 = verify_poem_sha512
					self.sha3_512 = verify_poem_sha3_512
					# Decrypt the base64 and use values

					poem_body_plain_file_uuid = uuid.uuid4()

					base64_plain_path = settings.MEDIA_ROOT + 'tmp/EftFileParser-whole-plain-' + str(poem_body_plain_file_uuid)
					base64_encrypted_path = settings.MEDIA_ROOT + 'tmp/EftFileParser-whole-encrypted-' + str(poem_body_plain_file_uuid)

					# print('EftFileParser.init -> self.eft_file_json[drm]: ' + self.eft_file_json['drm'])
					
					with open(base64_encrypted_path, 'w') as f_b64_p_p:
						f_b64_p_p.write(self.eft_file_json['drm'])
						f_b64_p_p.close()

					if os_path.isfile(base64_encrypted_path) and not os_path.isfile(base64_plain_path): 
						cmd = 'osprey_app-drm-inlovelikecom-v1 ' + self.source + ' "' + base64_encrypted_path + '" reverse > "' + base64_plain_path + '"'
						print('EftFileParser.init -> running command: ' + cmd)
						os.system(cmd)
					else:
						print('EftFileParser.init-> base64_plain_path file missing or base64_encrypted_path exists')

					base64_return = ''

					if os_path.isfile(base64_plain_path): 
						with open(base64_plain_path, 'r') as f_b64_e_p:
							self.base64_plain = f_b64_e_p.read().encode('utf-8').strip()
							f_b64_e_p.close()

					if self.base64_plain:
						# print('EftFileParser.init-> self.base64_plain: ' + str(self.base64_plain))
						self.base64_decoded = base64.b64decode(self.base64_plain.decode('utf-8')).decode("utf-8", "ignore") 
						
						if self.base64_decoded:
							# print('EftFileParser.init-> self.base64_decoded: ' + str(self.base64_decoded))
							self.base64_json = json.loads(self.base64_decoded)
						else:
							print('EftFileParser.init-> self.base64_decoded empty')
					else:
						print('EftFileParser.init-> self.base64_plain empty')
				# Uncompress poem in eft json object
				# Assign object values from uncompressed object

					self.poem_guid = self.base64_json['guid']
					self.poem_title = self.base64_json['title']
					self.poem_body = self.base64_json['body']

					self.file_tmp_path = self.__decode_poem_body_as_file()

					self.parsed = True
					print("EftFileParser -> Finished parsing eft_file")
				else:
					print("EftFileParser -> hash check failed")
		else:
			print("EftFileParser -> Version not supported or unverifyable: " + self.eft_file_json['version'])

	def __decode_poem_body_as_file(self):
		if not poem_tag_extras.poem_is_file(self.poem_title):
			print('decode_poem_body_as_file-> not a file')
			return False

		# Decrypt the base64 data and then convert to actual

		tmp_file_uuid = uuid.uuid4()
		
		if not os.path.isdir(settings.MEDIA_ROOT + 'tmp'):
			os.mkdir(settings.MEDIA_ROOT + 'tmp')
		
		base64_encrypted_path = settings.MEDIA_ROOT + 'tmp/EftFileParser-poem_body-encrypted-' + str(tmp_file_uuid)
		base64_plain_path = settings.MEDIA_ROOT + 'tmp/EftFileParser-poem_body-plain-' + str(tmp_file_uuid)
		base64_actual_path = settings.MEDIA_ROOT + 'tmp/EftFileParser-poem_body-actual-' + str(tmp_file_uuid)

		if self.poem_body:
			with open(base64_encrypted_path, 'w') as f:
				poem_body = self.poem_body
				poem_body = re.sub(r'(\r\n|\r|\n)', "", poem_body)
				f.write(poem_body)
				f.close()
		else:
			print('EftFileParser.__decode_poem_body_as_file -> self.poem_body is empty; not writing encrypted file')

		if os_path.isfile(base64_encrypted_path) and not os_path.isfile(base64_plain_path): 
			cmd = 'osprey_app-drm-inlovelikecom-v1 ' + self.source + ' "' + base64_encrypted_path + '" reverse > "' + base64_plain_path + '"'
			print('EftFileParser.__decode_poem_body_as_file -> running command: ' + cmd)
			os.system(cmd)
		else:
			print('EftFileParser.__decode_poem_body_as_file-> base64_plain_path file missing or base64_encrypted_path exists')

		if os_path.isfile(base64_plain_path): 
			with open(base64_plain_path, 'r') as f:
				self.base64_plain = f.read()
				f.close()

		# Convert the base64 into a regular file
		if not os_path.isfile(base64_actual_path): 
			cmd = 'base64 -d "' + base64_plain_path + '" > "' + base64_actual_path + '"'
			print('EftFileParser.__decode_poem_body_as_file -> running command: ' + cmd)
			os.system(cmd)
		else:
			print('EftFileParser.__decode_poem_body_as_file -> base64_actual_path exists: ' + base64_actual_path)

		base64_plain_path_mod = self.__convert_poem_body_file_extension(base64_actual_path)

		return base64_plain_path_mod

	def __convert_poem_body_file_extension(self, destination_path):
		cmd = 'file "' + destination_path + '"'
		print('EftFileParser.__convert_poem_body_file_extension -> running command: ' + cmd)
		file_type = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, executable="/bin/bash")
		file_type_return = self.__fix_file_type_return(str(file_type.stdout))
		print('EftFileParser.__convert_poem_body_file_extension -> command output: ' + file_type_return)

		extension = 'unknown'

		if 'JPEG' in file_type_return:
			extension = 'jpg'

		if 'M4A' in file_type_return:
			extension = 'm4a'

		if 'MP4' in file_type_return:
			extension = 'mp4'

		if 'GIF' in file_type_return:
			extension = 'gif'

		if 'PNG' in file_type_return:
			extension = 'png'

		if 'ASCII' in file_type_return:
			extension = 'txt'

		destination_path_new = destination_path + '.' + extension
		cmd = 'mv "' + destination_path + '" "' + destination_path_new + '"'
		print('EftFileParser.__convert_poem_body_file_extension -> running command: ' + cmd)
		os.system(cmd)

		return destination_path_new

	def __fix_file_type_return(self, input):
		file_type_return = input[2:][:-1]
		file_type_return = file_type_return if len(file_type_return) != 0 else False
		return file_type_return

	def is_valid_version(self):
		return self.version in self.supported_versions			

	def is_parsed(self):
		print("is parsed due to is_valid_version:" + str(self.is_valid_version()))
		print("is parsed due to is_verified:" + str(self.is_verified()))
		return self.parsed			

	def get_json(self):
		return self.eft_file_json

	def is_verified(self):
		self.verified = False

		if self.eft_file_json['source'] not in self.valid_sources:
			print("EftFileParser -> source not suppored: " + self.eft_file_json['source'])
			return self.verified

		if self.eft_file_json['source'] in self.white_list_sources:
			print("EftFileParser -> source is white listed: " + self.eft_file_json['source'])
			self.verified = True
			return self.verified

		url = self.eft_file_json['source'] + "poems/eft-verify/" + self.eft_file_json['sha512'] + '.json'
		try:
			response = urllib.request.urlopen(url)

			data = json.loads(response.read())

			if data.get('success'):
				self.verified = data.get('success')
		except Exception as e:
			print("EftFileParser -> is_verified Exception fetching data")
			print(e)

		if self.verified == False:
			print("EftFileParser -> is_verified unsuccessful")
		else:
			print("EftFileParser -> is_verified success")

		return self.verified

class EftPurchase(models.Model):
	imported_on = models.DateTimeField(blank=False, default=now)
	eft_file = models.TextField(null=False, blank=False)
	version = models.CharField(max_length=255, null=False, blank=False, default="1.0.0")
	drm = models.TextField(null=True, blank=True)
	base64_json = models.JSONField(null=True, blank=True)
	sha512 = models.CharField(max_length=255, null=True, blank=True)
	poem_guid = models.UUIDField(primary_key=False, editable=True, null=True, blank=True)
	poem_title = models.CharField(max_length=255, null=True, blank=True)
	poem_body = models.TextField(null=True, blank=True)
	owner = models.ForeignKey(User, related_name="eft_purchase_owner", null=True, blank=True, on_delete=models.CASCADE)
	valid = models.BooleanField(default=False)
	visible = models.BooleanField(default=True)
	source = models.CharField(max_length=255, null=True, blank=True)
	cost = models.DecimalField(null=True, blank=True, decimal_places=30, max_digits=65)
	resell_exclusion = models.BooleanField(default=False)
	guid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
	history = HistoricalRecords()
	file_cache = models.FileField(upload_to=settings.EFT_PURCHASE_FILE_PATH, null=True, blank=True)
	sha3_512 = models.CharField(max_length=255, null=True, blank=True)
	source_contract_address = models.CharField(max_length=255, null=True, blank=True)

	# Legacy 
	base64 = models.TextField(null=True, blank=True)

	def save(self, *args, **kwargs):
		if self.source and not self.source_contract_address:
			self.source_contract_address = self.get_source_contract_address()

		return super(EftPurchase, self).save(*args, **kwargs)

	def member(self):
		return self.owner.member_user.first()

	def offers(self):
		return self.eft_purchase_offer_eft_purchase.all()

	def file(self):
		return self.eft_resell_eft_purchase.order_by('created').reverse().first()

	def views(self):
		return len(self.page_view_eft_purchase.all())

	def get_source_contract_address(self):
		if self.source_contract_address:
			return self.source_contract_address

		if not self.source:
			return None 

		divide_me = DivideMe()
		address = divide_me.get_contract_address_for_host(self.source)

		self.source_contract_address = address
		self.save()

		return address

class EftPurchaseOffer(models.Model):
	guid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	name = models.CharField(max_length=255)
	email = models.CharField(max_length=255)
	comment = models.TextField(null=True, blank=True)
	created = models.DateTimeField(blank=False, default=now)
	sold = models.BooleanField(default=False)
	sold_on = models.DateTimeField(null=True, blank=True)
	history = HistoricalRecords()
	eft_purchase = models.ForeignKey(EftPurchase, related_name="eft_purchase_offer_eft_purchase", null=True, blank=True, on_delete=models.CASCADE)
	price = models.DecimalField(default=Decimal(0), decimal_places=30, max_digits=65)
	key = models.CharField(max_length=255, null=True, blank=True)
	destination = models.CharField(max_length=255, null=True, blank=True)
	contract_address = models.CharField(max_length=255, null=True, blank=True)
	destination_contract_address = models.CharField(max_length=255, null=True, blank=True)

	# Legacy
	offer_over_cost = models.DecimalField(null=True, blank=True, decimal_places=30, max_digits=65)

	def save(self, *args, **kwargs):
		if self.guid and not self.contract_address:
			self.contract_address = self.get_contract_address(True)

		if self.destination and not self.destination_contract_address:
			self.destination_contract_address = self.get_destination_contract_address(True)

		return super(EftPurchaseOffer, self).save(*args, **kwargs)

	def get_contract_address(self, skip_save):
		if not skip_save:
			skip_save = False

		if self.contract_address:
			return self.contract_address

		divide_me = DivideMe()
		address = divide_me.get_contract_address_for_system_over_uuid(self.guid)

		self.contract_address = address

		if not skip_save:
			self.save()

		return self.contract_address

	def get_destination_contract_address(self, skip_save):
		if self.destination_contract_address:
			return self.destination_contract_address

		divide_me = DivideMe()
		address = divide_me.get_contract_address_for_host(self.destination)

		self.destination_contract_address = address

		if not skip_save:
			self.save()

		return address


# from django.conf import settings
# from django.core.serializers.json import DjangoJSONEncoder
# from django.http import JsonResponse
# from django.utils import timezone
# import base64
# import hashlib
# import json


class EftFilePurchase:
	eft_purchase = EftPurchase
	cache = ''
	response = JsonResponse({})
	sha512 = ''
	drm = ''
	base64 = ''
	base64_decoded = ''
	base64_plain = ''
	base64_start = ''
	base64_json = ''
	json = ''
	cost = ''
	eft_json = ''
	source_history = []
	valid = False

	def __init__(self, eft_purchase, db_collation=None):
		self.eft_purchase = eft_purchase

		self._base64_decode_and_assign_json()

		self._rebuild_sources()

		self._rebuild_poem_body()

		self._encode_base64()

		self._calculate_cost()

		self._build_json()

		self._build_response()

		self._build_cache()

		self.valid = True

	def _build_json(self):
		divide_me = DivideMe()
		source_contract_address = divide_me.get_contract_address_for_host(settings.HTTPS_BASE_URL)
		member_contract_address = self.eft_purchase.member().contract_address

		self.eft_json = {
			'source': settings.HTTPS_BASE_URL,
			'source_contract_address': source_contract_address,
			'member_contract_address': member_contract_address,
			'version': settings.EFT_CURRENT_VERSION,
			'sha512': self.sha512,
			'sha3_512': self.sha3_512,
			'drm': self.drm,
			'cost': self.cost,
		}

	def _build_response(self):
		self.response = JsonResponse(self.eft_json)

	def _build_cache(self):
		self.cache = self.response.content.decode("utf-8")

	def _calculate_sha512(self):
		self.sha512 = hashlib.sha512(self.drm).hexdigest()

	def _calculate_sha3_512(self):
		self.sha3_512 = hashlib.sha3_512(self.drm).hexdigest()

	def _encode_base64(self):
		self.base64_plain = base64.b64encode(self._get_json_as_text().encode('utf-8'))
		# self.base64_plain = re.sub(r'(\r\n|\r|\n)', '', self.base64_plain.decode('utf-8'))

		# Decrypt the base64 data and then convert to actual

		tmp_file_uuid = uuid.uuid4()
		
		if not os.path.isdir(settings.MEDIA_ROOT + 'tmp'):
			os.mkdir(settings.MEDIA_ROOT + 'tmp')
		
		base64_plain_path = settings.MEDIA_ROOT + 'tmp/EftFilePurchase_e_b-plain-' + str(tmp_file_uuid)
		base64_encrypted_path = settings.MEDIA_ROOT + 'tmp/EftFilePurchase_e_b-encrypted-' + str(tmp_file_uuid)
		if self.base64_plain:
			with open(base64_plain_path, 'w') as f:
				f.write(self.base64_plain.decode('utf-8').strip())
				f.close()
		else:
			print('EftFilePurchase -> empty base64_plain')

		if os_path.isfile(base64_plain_path) and not os_path.isfile(base64_encrypted_path): 
			cmd = 'osprey_app-drm-inlovelikecom-v1 ' + settings.HTTPS_BASE_URL + ' "' + base64_plain_path + '" > "' + base64_encrypted_path + '"'
			print('EftFilePurchase -> running command: ' + cmd)
			os.system(cmd)
		else:
			print('EftFilePurchase-> base64_plain_path file missing or base64_encrypted_path exists')

		if os_path.isfile(base64_encrypted_path): 
			with open(base64_encrypted_path, 'r') as f:
				self.drm = f.read().encode('utf-8').strip()
				f.close()

		self._calculate_sha512()
		self._calculate_sha3_512()

		self.drm = self.drm.decode('utf-8')

	def _get_json_as_text(self):
		poem_json_text = json.dumps(self.json, cls=DjangoJSONEncoder, default = str)
		return poem_json_text

	def _calculate_cost(self):
		print("EftFilePurchase._calculate_cost -> length of base64: " + str(len(self.drm)))
		self.cost = str(len(self.drm) * settings.EFT_COST_PER_BYTE)
		print("EftFilePurchase._calculate_cost -> cost: " + str(self.cost))

	def _base64_decode_and_assign_json(self):
		tmp_file_uuid = uuid.uuid4()
		
		if not os.path.isdir(settings.MEDIA_ROOT + 'tmp'):
			os.mkdir(settings.MEDIA_ROOT + 'tmp')
		
		base64_plain_path = settings.MEDIA_ROOT + 'tmp/EftFilePurchase_b_d_a_a_j-plain-' + str(tmp_file_uuid)
		base64_encrypted_path = settings.MEDIA_ROOT + 'tmp/EftFilePurchase_b_d_a_a_j-encrypted-' + str(tmp_file_uuid)

		if self.eft_purchase.drm:
			with open(base64_encrypted_path, 'w') as f:
				f.write(self.eft_purchase.drm.strip())
				f.close()
		else:
			print('EftFilePurchase -> empty eft_purchase.drm')

		# Decrypt the base64 data and then convert to actual

		if os_path.isfile(base64_encrypted_path) and not os_path.isfile(base64_plain_path): 
			cmd = 'osprey_app-drm-inlovelikecom-v1 ' + self.eft_purchase.source + ' "' + base64_encrypted_path + '" reverse > "' + base64_plain_path + '"'
			print('EftFilePurchase -> running command: ' + cmd)
			os.system(cmd)
		else:
			print('EftFilePurchase-> base64_plain_path file missing or base64_encrypted_path exists')

		if os_path.isfile(base64_plain_path): 
			with open(base64_plain_path, 'r') as f:
				self.base64_start = f.read().strip()
				f.close()

		if self.base64_start:
			self.base64_decoded = base64.b64decode(self.base64_start).decode("utf-8", "ignore") 
			self.base64_json = json.loads(self.base64_decoded)
			self.json = self.base64_json
		else:
			print('EftFilePurchase -> error. self.base64_start is empty')

	def _rebuild_sources(self):
		if self.base64_json.get('source_history'):
			self.source_history = self.base64_json['source_history']
		else:
			self.source_history = []

		divide_me = DivideMe()
		source_contract_address = divide_me.get_contract_address_for_host(self.eft_purchase.source)
		member_contract_address = self.eft_purchase.member().contract_address

		source_history_record = {
			'source': self.eft_purchase.source,
			'source_contract_address': source_contract_address,
			'member_contract_address': member_contract_address,
			'cost': str(self.eft_purchase.cost),
			'sha512': self.eft_purchase.sha512,
			'sha3_512': self.eft_purchase.sha3_512,
			'created_on': str(timezone.now()),
		}

		self.source_history.append(source_history_record)
		
		self.json['source'] = settings.HTTPS_BASE_URL
		self.json['source_history'] = self.source_history

	def _rebuild_poem_body(self):
		if not poem_tag_extras.poem_is_file(self.json['title']):
			print('_rebuild_poem_body-> not a file')
			self.poem_body_plain = self.json['body']
			return False

		decode_uuid = uuid.uuid4()
		
		if not os.path.isdir(settings.MEDIA_ROOT + 'tmp'):
			os.mkdir(settings.MEDIA_ROOT + 'tmp')
		
		decode_poem_body_base64_plain_path = settings.MEDIA_ROOT + 'tmp/EftFilePurchase_r_p_b-decode-plain-' + str(decode_uuid)
		decode_poem_body_base64_encrypted_path = settings.MEDIA_ROOT + 'tmp/EftFilePurchase_r_p_b-decode-encrypted-' + str(decode_uuid)

		if self.json['body']:
			with open(decode_poem_body_base64_encrypted_path, 'w') as f:
				f.write(self.json['body'].strip())
				f.close()
		else:
			print('EftFilePurchase._rebuild_poem_body -> empty json[body]')

		# Decrypt the base64 data and then convert to actual

		if os_path.isfile(decode_poem_body_base64_encrypted_path) and not os_path.isfile(decode_poem_body_base64_plain_path): 
			cmd = 'osprey_app-drm-inlovelikecom-v1 ' + self.eft_purchase.source + ' "' + decode_poem_body_base64_encrypted_path + '" reverse > "' + decode_poem_body_base64_plain_path + '"'
			print('EftFilePurchase._rebuild_poem_body -> running command: ' + cmd)
			os.system(cmd)
		else:
			print('EftFilePurchase._rebuild_poem_body-> decode_poem_body_base64_encrypted_path file missing or decode_poem_body_base64_plain_path exists')

		if os_path.isfile(decode_poem_body_base64_plain_path): 
			with open(decode_poem_body_base64_plain_path, 'r') as f:
				self.poem_body_plain = f.read().strip()
				f.close()

		encode_poem_body_base64_encrypted_path = settings.MEDIA_ROOT + 'tmp/EftFilePurchase_r_p_b-encode-plain-' + str(decode_uuid)

		if os_path.isfile(decode_poem_body_base64_plain_path) and not os_path.isfile(encode_poem_body_base64_encrypted_path): 
			cmd = 'osprey_app-drm-inlovelikecom-v1 ' + settings.HTTPS_BASE_URL + ' "' + decode_poem_body_base64_plain_path + '" > "' + encode_poem_body_base64_encrypted_path + '"'
			print('EftFilePurchase._rebuild_poem_body -> running command: ' + cmd)
			os.system(cmd)
		else:
			print('EftFilePurchase._rebuild_poem_body-> decode_poem_body_base64_plain_path file missing or encode_poem_body_base64_encrypted_path exists')

		if os_path.isfile(encode_poem_body_base64_encrypted_path): 
			with open(encode_poem_body_base64_encrypted_path, 'r') as f:
				self.json['body'] = f.read().strip()
				f.close()

	def is_valid(self):
		return self.valid

	def get_cache(self):
		return self.cache

	def get_response(self):
		return self.response

	def get_eft_purchase(self):
		return self.eft_purchase



class EftResell(models.Model):
	created = models.DateTimeField(blank=False, default=now)
	eft_purchase = models.ForeignKey(EftPurchase, related_name="eft_resell_eft_purchase", on_delete=models.CASCADE)
	cache = models.TextField(null=True, blank=True)
	guid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
	sha512 = models.CharField(max_length=255, null=True, blank=True)
	drm = models.TextField(null=True, blank=True)
	version = models.CharField(max_length=255, null=True, blank=True, default=settings.EFT_CURRENT_VERSION)
	cost = models.DecimalField(null=True, blank=True, decimal_places=30, max_digits=65)
	history = HistoricalRecords()
	destination = models.CharField(max_length=255, null=True, blank=True)
	base64_json = models.JSONField(null=True, blank=True)
	sha3_512 = models.CharField(max_length=255, null=True, blank=True)

	# Legacy
	key = models.CharField(max_length=255, null=True, blank=True)
	base64 = models.TextField(null=True, blank=True)

	def is_free(self):
		return poem_tag_extras.poem_is_free(self.eft_purchase.poem_title)

	def base64_json_as_var(self):
		if not self.base64_json:
			return {}

		if type(self.base64_json) == dict:
			return self.base64_json

		try:
			json_data = json.loads(self.base64_json)
		except:
			json_data = {}

		return json_data

class DivideMe:
	def get_contract_address_for_host(self, host):
	# offer_contract_address = str(hex(offer_guid_as_int))
		cmd = 'osprey_app_divide_me-inlovelike-v1 1 ' + host
		diff = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, executable="/bin/bash")
		diff_return = str(diff.stdout.read())
		diff.terminate()

		host_sha_digest = hashlib.sha512(diff_return.encode('utf-8')).digest()
		host_sha_as_int = int.from_bytes(host_sha_digest, 'big')
		
		address = str(hex(host_sha_as_int))

		return address

	def get_contract_address_for_system_over_uuid(self, guid):
		offer_guid = uuid.UUID(str(guid))
		offer_guid_as_int = int.from_bytes(offer_guid.bytes, 'big')

		cmd = 'osprey_app_divide_me-inlovelike-v1 1 ' + settings.HTTPS_BASE_URL
		diff = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, executable="/bin/bash")
		diff_return = str(diff.stdout.read())
		diff.terminate()

		host_sha_digest = hashlib.sha512(diff_return.encode('utf-8')).digest()
		host_sha_as_int = int.from_bytes(host_sha_digest, 'big')
		
		return str(hex(offer_guid_as_int + host_sha_as_int))


# Sources:
# https://stackoverflow.com/questions/21614941/django-models-user-id
# https://stackoverflow.com/questions/34305805/django-foreignkeyuser-in-models
# https://docs.djangoproject.com/en/3.1/topics/db/models/
# https://docs.djangoproject.com/en/3.2/ref/models/fields/#integerfield
# https://stackoverflow.com/questions/59639092/django-decimalfield-what-is-the-maximum-value-of-max-digits
# https://stackoverflow.com/questions/2569015/django-floatfield-or-decimalfield-for-currency
# https://stackoverflow.com/questions/3090302/how-do-i-get-the-object-if-it-exists-or-none-if-it-does-not-exist