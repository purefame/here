from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.utils import timezone
from poems.models import EftPurchase, EftFileParser, EftResell, EftFilePurchase
from poems.templatetags import poem_tag_extras
from pages.models import Rss2
from django.utils.timezone import now
import re
import hashlib
import logging
import os.path as os_path
import os

logger = logging.getLogger(__name__)

@receiver(post_save, sender=EftPurchase, dispatch_uid="eft.eft_purchase_post_save")
def eft_purchase_post_save(sender, instance, created, **kwargs):
	if instance.eft_file and instance.sha512 is None:
		print('EftPurchase.eft_purchase_post_save - > instance.eft_file and instance.sha512 is None')
		try:
			eft_purchase_file = EftFileParser(instance.eft_file)
			if eft_purchase_file.is_parsed():
				print('EftPurchase.eft_purchase_post_save - > eft_purchase_file.is_parsed()')
				instance.poem_guid = eft_purchase_file.poem_guid
				instance.poem_title = eft_purchase_file.poem_title
				instance.poem_body = eft_purchase_file.poem_body
				instance.base64_json = eft_purchase_file.base64_json
				instance.drm = eft_purchase_file.drm
				instance.sha512 = eft_purchase_file.sha512
				instance.sha3_512 = eft_purchase_file.sha3_512
				instance.version = eft_purchase_file.version
				instance.source = eft_purchase_file.source
				instance.cost = eft_purchase_file.cost
				# Write the file to cache

				if not os.path.isdir(settings.MEDIA_ROOT + 'eft_purchases/'):
					os.mkdir(settings.MEDIA_ROOT + 'eft_purchases/')

				if eft_purchase_file.file_tmp_path:
					print('EftPurchase.eft_purchase_post_save - > eft_purchase_file.file_tmp_path')
					new_name = eft_purchase_file.file_tmp_path.split("/")[-1]
					new_path = settings.MEDIA_ROOT + 'eft_purchases/' + new_name

					if os.path.isfile(eft_purchase_file.file_tmp_path) and new_path != eft_purchase_file.file_tmp_path:
						os.rename(eft_purchase_file.file_tmp_path, new_path)
						instance.file_cache = 'eft_purchases/' + new_name

				instance.valid = True
				instance.save()
			else:
				print('EftPurchase.eft_purchase_post_save -> Unable to parse data in eft_purchase_file')
		except Exception as e:
			print('EftPurchase.eft_purchase_post_save - > Exception on EftFileParser')
			print(e)

	if instance.file():
		eft_resell = instance.file()

		try:
			eft_purchase_file = EftFilePurchase(instance)
		except Exception as e:
			print('EftPurchase.eft_purchase_post_save - > Exception eft_purchase_file with first')
			print(e)
			logger.info('EftPurchase.eft_purchase_post_save -> Exception')
			logger.info(str(e))
			eft_purchase_file = False
		
		if eft_purchase_file and eft_purchase_file.is_valid():
			print('EftPurchase.eft_purchase_post_save - > eft_purchase_file and eft_purchase_file.is_valid()')
			eft_resell.cache = eft_purchase_file.get_cache()
			eft_resell.sha512 = eft_purchase_file.sha512
			eft_resell.sha3_512 = eft_purchase_file.sha3_512
			eft_resell.drm = eft_purchase_file.drm
			eft_resell.base64_json = eft_purchase_file.base64_json
			eft_resell.cost = eft_purchase_file.cost
		else:
			print('EftPurchase.eft_purchase_post_save - > failed eft_purchase or eft_purchase_file.is_valid()')

		eft_resell.version = settings.EFT_CURRENT_VERSION
		eft_resell.key = hashlib.sha512(str(timezone.now()).encode('utf-8')).hexdigest()
		eft_resell.save()
	else:
		eft_resell = EftResell(eft_purchase=instance)

		try:
			eft_purchase_file = EftFilePurchase(instance)
		except Exception as e:
			print('EftPurchase.eft_purchase_post_save - > Exception eft_purchase_file with new EftResell')
			print(e)
			eft_purchase_file = False

		if eft_purchase_file and eft_purchase_file.is_valid():
			print('EftPurchase.eft_purchase_post_save - > eft_purchase_file and eft_purchase_file.is_valid()')
			eft_resell.cache = eft_purchase_file.get_cache()
			eft_resell.sha512 = eft_purchase_file.sha512
			eft_resell.sha3_512 = eft_purchase_file.sha3_512
			eft_resell.drm = eft_purchase_file.drm
			eft_resell.base64_json = eft_purchase_file.base64_json
			eft_resell.cost = eft_purchase_file.cost
		else:
			print('EftPurchase.eft_purchase_post_save - > failed eft_purchase or eft_purchase_file.is_valid()')

		eft_resell.version = settings.EFT_CURRENT_VERSION
		eft_resell.save()


# Sources:
# https://docs.djangoproject.com/en/3.1/ref/signals/
# https://stackoverflow.com/questions/43145712/calling-a-function-in-django-after-saving-a-model
# https://docs.djangoproject.com/en/3.2/ref/signals/#post-save
# https://stackoverflow.com/questions/45859465/django-signals-not-receiving-working