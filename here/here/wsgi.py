"""
WSGI config for here project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

ENV = "production" if os.uname().nodename == 'purefame-plesk01' else "development"

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'here.settings_' + ENV)

application = get_wsgi_application()