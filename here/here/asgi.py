"""
ASGI config for here project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

ENV = "production" if os.uname().nodename == 'purefame-plesk01' else "development"

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'here.settings_' + ENV)


application = get_asgi_application()
