"""here URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.auth import views as auth_views
from django.views.generic.base import RedirectView
from django.conf import settings
from django.conf.urls.static import static

# https://docs.djangoproject.com/en/3.1/ref/urls/#handler403
from django.conf.urls import handler400, handler403, handler404, handler500
from pages import views as page_views
from members import views as member_views
from poems import views as poem_views

urlpatterns = [
    path("", page_views.index, name="index"),
    path('sitemap.xml', page_views.sitemap, name="sitemap"),
    path('sitemaps.xml', page_views.sitemap),
    path('admin/', admin.site.urls),
    re_path(r'^profile-.*-(?P<user_id>\d+)\.html$', member_views.profile, name="member_profile"),
    re_path(r'^profile-(?P<user_id>\d+)\.html$', member_views.profile, name="member_profile_id"),
   	path(
        "favicon.ico",
        RedirectView.as_view(url=staticfiles_storage.url("favicon.ico")),
    ),
    path('login.html',auth_views.LoginView.as_view(template_name='members/login.html'), name='login'),
    path('logout.html', auth_views.LogoutView.as_view(template_name="members/logged_out.html"), name='logout'),
    path('password-change/', auth_views.PasswordChangeView.as_view(template_name="accounts/password_change_form.html"), name="password_change"),
    path('password-change-done/', auth_views.PasswordChangeDoneView.as_view(template_name="accounts/password_change_done.html"), name="password_change_done"),
    path('members/', include('members.urls')),
    # path('search.html', poem_views.search_get, name="poem_search_get"),
    path('rss2.xml', page_views.rss2, name="rss2"),
    path('robots.txt', page_views.robots, name="robots"),
    # path('search_advanced.html', poem_views.search_advanced, name="search_advanced"),
    path("register-eft.html", member_views.register_eft, name="register_eft"),
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name="accounts/password_reset_form.html", from_email=settings.EMAIL_FROM), name='password_reset'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="accounts/password_reset_confirm.html"), name='password_reset_confirm'),
    path('password-reset-done/', auth_views.PasswordResetDoneView.as_view(template_name="accounts/password_reset_done.html"), name='password_reset_done'),
    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name="accounts/password_reset_complete.html"), name='password_reset_complete'),
    re_path(r'^eft-(?P<sha512>\w+)\.json$', poem_views.eft_get, name="root_eft"),
    path('poems/', include('poems.urls')),
    path('search.html', poem_views.search_get, name="poem_search_get"),
    path('search_advanced.html', poem_views.search_advanced, name="search_advanced"),
]

handler400 = page_views.catch_redirect
handler403 = page_views.catch_redirect
handler404 = page_views.catch_redirect
handler500 = page_views.catch_redirect


# Sources:
# https://docs.djangoproject.com/en/3.1/topics/auth/default/
