"""
Django settings for here project.

Generated by 'django-admin startproject' using Django 3.2.2.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""
import os
from pathlib import Path
import environ

env = environ.Env()
environ.Env.read_env()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'pages',
    'poems.apps.PoemsConfig',
    'simple_history',
    'members',
    'captcha',
    'corsheaders',
]


MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',
]

ROOT_URLCONF = 'here.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')], # new
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'pages.context_processors.seo_title_processor',
                'pages.context_processors.seo_description_processor',
            ],
            'libraries':{
            },

        },
    },
]


WSGI_APPLICATION = 'here.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('DB_NAME'),
        'USER': env('DB_USERNAME'),
        'PASSWORD': env('DB_PASSWORD'),
        'HOST': env('DB_HOST'),
        'PORT': '5432',
        'CONN_MAX_AGE': 28800,
        'OPTIONS': {
            'options': '-c search_path=' + env('DB_NAME'),
        },
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


RECAPTCHA_PUBLIC_KEY = "6LdbTpIaAAAAALZuF4GJWUwoOghMLkP2W4neNzNu"
RECAPTCHA_PRIVATE_KEY = "6LdbTpIaAAAAALZuF4GJWUwoOghMLkP2W4neNzNu"

MOD_MIN_STR_DATE_YYYYMMDDHHMM = '2021/03/30 16:46'

# More SEARCH_* found within static/theme.js
SEARCH_PLUS_TITLE_CLASS = 'plus fas fa-search-plus btn btn-lg btn-light'
SEARCH_PLUS_BODY_CLASS = 'plus fas fa-search-plus btn btn-lg btn-light'

LOGIN_URL = '/login.html' 
LOGOUT_URL = '/logout.html'
LOGIN_REDIRECT_URL = '/members/welcome.html'

MEMBER_PRICING_FREE = 'free'
MEMBER_PRICING_PRO = 'professional'
MEMBER_PRICING_UNLIMITED = 'unlimited'
MEMBER_PRICING_BASIC = 'basic'
MEMBER_PRICING_EFT = 'eft'

PRICING_CHOICES = (
    (MEMBER_PRICING_EFT,'Electronic File Tokens'),
    (MEMBER_PRICING_BASIC,'Basic'),
    (MEMBER_PRICING_FREE,'Free'),
    (MEMBER_PRICING_PRO, 'Professional'),
    (MEMBER_PRICING_UNLIMITED,'Unlimited'),
)

MEMBER_STANDING_PENDING = 'pending'
MEMBER_STANDING_PAID = 'paid'
MEMBER_STANDING_CANCELLED = 'cancelled'

STANDING_CHOICES = (
    (MEMBER_STANDING_PENDING,'Pending'),
    (MEMBER_STANDING_PAID, 'Paid'),
    (MEMBER_STANDING_CANCELLED,'Cancelled'),
)

MEMBER_POEMS_PRO = 100

POEM_ITEM_SEARCH_CHOICES = (
    ('show','Show'),
    ('hide', 'Hide'),
)

POEM_ITEM_SEARCH_DEFAULT_CHOICE = 'hide'

HOME_SCREEN_CHOICES = (
    ('welcome','Welcome'),
    ('profile', 'Profile'),
    ('eft_purchases', 'EFT Purchases'),
    ('eft_offers', 'EFT Offers'),
)

HOME_SCREEN_DEFAULT_CHOICE = 'welcome'

SHOW_DIFF_ON_POEM_ITEMS_CHOICES = (
    ('show','Show'),
    ('hide', 'Hide'),
)

SHOW_DIFF_ON_POEM_ITEMS_DEFAULT_CHOICE = 'hide'

SELL_ALL_POEMS_AS_EFT_CHOICES = (
    ('agree','Agree'),
    ('deny', 'Deny'),
)

SELL_ALL_POEMS_AS_EFT_DEFAULT_CHOICE = 'agree'

RECAPTCHA_PUBLIC_KEY = env('RECAPTCHA_PUBLIC_KEY')
RECAPTCHA_PRIVATE_KEY = env('RECAPTCHA_PRIVATE_KEY')

FEATURE_CAPTCHA = False

POEM_TITLE_SHORT_LENGTH = 14
POEM_TITLE_TWITTER_LENGTH = 25
POEM_BODY_SHORT_LENGTH = 82

BOOK_IMAGE_TYPES = ['image']
# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160
BOOK_IMAGE_MAX_UPLOAD_SIZE = "5242880"

MEDIA_ROOT = os.path.join(BASE_DIR, 'media/') # 'data' is my media folder
MEDIA_URL = '/media/'

DJANGORESIZED_DEFAULT_SIZE = [150, 150]
DJANGORESIZED_DEFAULT_QUALITY = 75
DJANGORESIZED_DEFAULT_KEEP_META = True
DJANGORESIZED_DEFAULT_FORCE_FORMAT = 'JPEG'
DJANGORESIZED_DEFAULT_FORMAT_EXTENSIONS = {'JPEG': ".jpg", 'PNG': '.png'}
DJANGORESIZED_DEFAULT_NORMALIZE_ROTATION = True

DATA_UPLOAD_MAX_MEMORY_SIZE = 1073741274 # 1GB
FILE_UPLOAD_MAX_MEMORY_SIZE = 1073741274 # 1GB

HTTPS_BASE_URL = 'https://here.inlovelike.com/'

EMAIL_FROM = 'nate@purefame.com'
EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

EFT_CURRENT_VERSION = '3.0.0'
EFT_PURCHASE_VALID_SOURCES = ['https://inlovelike.com/', 'https://here.d.inlovelike.com', HTTPS_BASE_URL, 'https://d.inlovelike.com/']
EFT_PURCHASE_WHITE_LIST_SOURCES = ['https://here.d.inlovelike.com/', 'https://d.inlovelike.com/']

# CORS_ALLOW_ALL_ORIGINS = True
CORS_ALLOWED_ORIGINS = ['https://here.d.inlovelike.com', 'https://inlovelike.com', 'https://d.inlovelike.com', HTTPS_BASE_URL[:-1]]
CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
]

EFT_PURCHASE_SOURCES_CHOICES = (
    (HTTPS_BASE_URL, 'inLoveLike Here Development EFT Server - Secured V1W1'),
    ('https://d.inlovelike.com/', 'inLoveLike Development EFT Server - Secured V1W1'),
    ('https://inlovelike.com/', 'inLoveLike EFT Server - Secured V2'),
    ('https://here.inlovelike.com/', 'inLoveLike Here EFT Server - Secured V2'),
)

EMAIL_HOST = env('SMTP_HOST')
EMAIL_HOST_USER = env('SMTP_USERNAME')
EMAIL_HOST_PASSWORD = env('SMTP_PASSWORD')
EMAIL_PORT = env('SMTP_PORT')
EMAIL_USE_TLS = True

EMAIL_FROM = 'nate@purefame.com'
# EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"
SENDGRID_SANDBOX_MODE_IN_DEBUG = False
SENDGRID_API_KEY = env('SENDGRID_API_KEY')

# LOGGING = {
#     'version': 1,
#     # Version of logging
#     'disable_existing_loggers': False,
#     #disable logging 
#     # Handlers #############################################################
#     'handlers': {
#         'file': {
#             'level': 'INFO',
#             'class': 'logging.FileHandler',
#              'filename': '/home/nate/OneDrive/code/inlovelike.com/logs/here.d.inlovelike.com-django-debug.log',
#         },
# ########################################################################
#         'console': {
#             'class': 'logging.StreamHandler',
#         },
#     },
#     # Loggers ####################################################################
#     'loggers': {
#         'django': {
#             'handlers': ['file', 'console'],
#             'level': 'INFO',
#             'propagate': True,
#             'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG')
#         },
#     },
# }


TIPS_CHOICES = (
    ('verbose','Verbose'),
    ('some', 'Some'),
    ('none', 'None')
)

TIPS_DEFAULT_CHOICE = 'verbose'

# up to 30 decimal places to find the truth. 
# History:
#   0.0000001
#   0.0000001011 
#   0.0000000005555555555555555555
#   0.0000000001822079314040729
EFT_COST_PER_BYTE = 0.000000001822079314040729

EFT_SUPPORTED_VERSIONS = ['1.0.0', '1.1.0', '1.2.0', '2.0.0', '3.0.0']

SECURE_SSL_REDIRECT = True

# https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/#https
# https://docs.djangoproject.com/en/3.0/topics/security/
# This instructs the browser to only send these cookies over HTTPS connections.
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

EFT_PURCHASE_FILE_PATH = 'eft_purchases/'


TRANSFER_KEY_TYPE_CHOICES = (
    ('uuid','UUID4'),
    ('sha512', 'SHA2-512'),
    ('sha3_512', 'SHA3-512'),
)

TRANSFER_KEY_TYPE_DEFAULT_CHOICE = 'sha512'
