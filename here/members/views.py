from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, update_session_auth_hash, login
from django.contrib.auth.tokens import default_token_generator    
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import redirect
from members.forms import MemberPoemForm, MemberProfessionalForm, MemberProfileForm, MemberProfessionalProfileForm, BookForm, MemberBasicForm, UserForm
from members.models import Member, Book
from django.conf import settings
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

# from django.db.models import Q, OuterRef
# from django.contrib.admin.views.decorators import staff_member_required
# import re
# import subprocess
# import functools
# import operator

def list(request):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

    members_not_cancelled_q = Member.objects.exclude(standing=settings.MEMBER_STANDING_CANCELLED).values_list('user_id', flat=True)
    members_not_cancelled = []
    for user_id in members_not_cancelled_q:
        members_not_cancelled.append(user_id)
        
    members = User.objects.filter(is_staff=False, id__in=members_not_cancelled)
    staff = User.objects.filter(is_staff=True, id__in=members_not_cancelled)

    options = {
        'members': members,
        'staff': staff,
        'seo_title': 'Members tally of how many members - inLoveLike - Poetry with Poems',
        'seo_description': 'You may be curious from time to time how many people we have with us as Poets.',
    }
    return render(request, 'members/list.html', options)

def profile(request, user_id):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

    profile_user = get_object_or_404(User, pk=user_id)

    member = profile_user.member_user.first()
    if member.standing == settings.MEMBER_STANDING_CANCELLED:
        response = redirect('/?error=profile_cancelled')
        return response

    if profile_user.first_name or profile_user.last_name:
        profile_user_title = " ".join([profile_user.first_name, profile_user.last_name])
    elif profile_user.username:
        profile_user_title = profile_user.username
    else:
        profile_user_title = profile_user.email

    eft_purchases = profile_user.eft_purchase_owner.filter(visible=True)

    options = {
        'seo_title': profile_user_title + ' Poet Profile - inLoveLike - Poetry with Poems',
        'seo_description': 'Find ' + profile_user_title + ' on inLoveLike with a Poet membership profile and see all the activity such as the Poems made manifest.',
        'profile_user': profile_user,
        'profile_user_title': profile_user_title,
        'eft_purchases': eft_purchases,
        'error' : request.GET.get('error'),
        'success' : request.GET.get('success'),
        'books': profile_user.book_user.all(),
    }
    return render(request, 'members/profile.html', options)

@login_required
def edit(request):
    member = request.user.member_user.first()
    poem_item_search = member.ux_poem_item_search if request.POST.get('ux_poem_item_search') is None else request.POST.get('ux_poem_item_search')
    home_screen = request.POST.get('ux_home_screen', member.ux_home_screen)
    ux_tips = request.POST.get('ux_tips', member.ux_tips)
    bio = request.POST.get('bio', member.bio)
    website = request.POST.get('website', member.website)
    member_profile_form_data = {
        'ux_poem_item_search' : poem_item_search,
        'ux_home_screen': home_screen,
        'ux_tips': ux_tips,
    }

    show_diff_on_poem_items = request.POST.get('ux_show_diff_on_poem_items', member.ux_show_diff_on_poem_items)
    sell_all_poems_as_eft = request.POST.get('ux_sell_all_poems_as_eft', member.ux_sell_all_poems_as_eft)
    member_professional_profile_form_data = {
        'bio': bio,
        'website': website,
        'ux_show_diff_on_poem_items' : show_diff_on_poem_items,
        'ux_sell_all_poems_as_eft' : sell_all_poems_as_eft,
    }
    form_member_professional_profile = False
    if request.method == 'POST':
        form = MemberProfileForm(member_profile_form_data)
        if member.pricing == settings.MEMBER_PRICING_PRO or member.pricing == settings.MEMBER_PRICING_UNLIMITED:
            form_member_professional_profile = MemberProfessionalProfileForm(member_professional_profile_form_data)
            form_member_professional_profile_is_valid = form_member_professional_profile.is_valid()
        else:
            form_member_professional_profile_is_valid = True

        if form.is_valid() and form_member_professional_profile_is_valid:
            # Attempt to save the member
            try:
                member.ux_poem_item_search = form.cleaned_data["ux_poem_item_search"]
                member.ux_home_screen = form.cleaned_data["ux_home_screen"]
                member.ux_tips = form.cleaned_data["ux_tips"]
                
                if member.pricing == settings.MEMBER_PRICING_PRO or member.pricing == settings.MEMBER_PRICING_UNLIMITED:
                    member.bio = form_member_professional_profile.cleaned_data["bio"]
                    member.website = form_member_professional_profile.cleaned_data["website"]
                    member.ux_show_diff_on_poem_items = form_member_professional_profile.cleaned_data["ux_show_diff_on_poem_items"]
                    member.ux_sell_all_poems_as_eft = form_member_professional_profile.cleaned_data["ux_sell_all_poems_as_eft"]
                
                member.save()

                response = redirect('/profile-' + str(request.user.id) + '.html?success=save')
                return response
            except Exception as e:
                # something went wrong saving the member
                print("Exception occurred")
                print(e)
                response = redirect('/profile-' + str(request.user.id) + '.html?error=saved')
                return response
    else:
        form = MemberProfileForm(initial=member_profile_form_data)
        if member.pricing == settings.MEMBER_PRICING_PRO or member.pricing == settings.MEMBER_PRICING_UNLIMITED:
            form_member_professional_profile = MemberProfessionalProfileForm(member_professional_profile_form_data)


    user_form_data = {
        'first_name': request.user.first_name,
        'last_name': request.user.last_name,
        'username': request.user.username,
        'email': request.user.email,
    }
    user_form = UserForm(user_form_data)

    options = {
        'seo_title': 'Edit Your Profile - inLoveLike - Poetry with Poems',
        'seo_description': 'Edit your profile and get a customized interface with inLoveLike.',
        'form': form,
        'user_form': user_form,
        'member': member,
        'books': request.user.book_user.all(),
        'form_member_professional_profile': form_member_professional_profile,
    }
    return render(request, 'members/edit.html', options)

def edit_update(request):
    if request.method == 'POST':
        user_form_data = {
            'first_name': request.POST.get('first_name', request.user.first_name),
            'last_name': request.POST.get('last_name', request.user.last_name),
            'username': request.POST.get('username', request.user.username),
            'email': request.POST.get('email', request.user.email),
        }
        user_form = UserForm(user_form_data)
        if user_form.is_valid():
            user = request.user
            user.first_name = user_form.cleaned_data['first_name']
            user.last_name = user_form.cleaned_data['last_name']
            user.username = user_form.cleaned_data['username']
            user.email = user_form.cleaned_data['email']
            
            try:
                user.save()
                response = redirect('/members/edit.html?success=updated')
                return response
            except Exception as e:            
                response = redirect('/members/edit.html?error=update')
                return response
    

    response = redirect('/members/edit.html?error=edit_update')
    return response

@login_required
def add_book(request):
    member = request.user.member_user.first()
    books = request.user.book_user.all()

    if member.pricing != settings.MEMBER_PRICING_PRO and member.pricing != settings.MEMBER_PRICING_UNLIMITED and len(books) >= 1:
        print('members:add_book -> member is not PRO or UNLIMITED -> limit of 1 reached')
        response = redirect('/profile-' + str(request.user.id) + '.html?error=add_book_limit')
        return response

    book_title = request.POST.get('title') if request.method == 'POST' else ''
    book_url = request.POST.get('url') if request.method == 'POST' else ''
    book_image = request.POST.get('image') if request.method == 'POST' else ''
    member_book_form_data = {
        'title' : book_title,
        'url': book_url,
        'image': book_image,
        'user': request.user,
    }

    if request.method == 'POST':
        form = BookForm(member_book_form_data, request.FILES)
        if member.pricing == settings.MEMBER_PRICING_PRO or member.pricing == settings.MEMBER_PRICING_UNLIMITED:
            print('members:add_book -> PRO OR UNLIMITED: No limit on number of books')
        else:
            print('members:add_book -> NOT PRO OR UNLIMITED: Check how many books are added')

        if form.is_valid():
            # Attempt to save the member
            try:
                # Book.objects.create(user=request.user, title=form.cleaned_data["title"], url=form.cleaned_data["url"], image=request.FILES["image"])
                book = Book(user=request.user, title=form.cleaned_data["title"], url=form.cleaned_data["url"], image=request.FILES["image"])
                book.save()
                
                response = redirect('/profile-' + str(request.user.id) + '.html?success=add_book')
                return response
            except Exception as e:
                # something went wrong saving the member
                print('members:add_book -> form.is_valid() -> Book.create -> Exception -> ' + str(e))
                response = redirect('/profile-' + str(request.user.id) + '.html?error=add_book')
                return response
    else:
        form = BookForm(initial=member_book_form_data)

    options = {
        'seo_title': 'Add a Book to Your Profile - inLoveLike - Poetry with Poems',
        'seo_description': 'Customize your profile by adding a book that your poetry may also reside in with inLoveLike.',
        'form': form,
        'member': member,
    }
    return render(request, 'members/add_book.html', options)


@login_required
def delete_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    if request.user != book.user:
        response = redirect('/profile-' + str(request.user.id) + '.html?error=delete_book')
        return response

    try:
        book.delete()
        response = redirect('/profile-' + str(request.user.id) + '.html?success=delete_book')
        return response
    except Exception as e:
        # something went wrong saving the member
        print(e)
        response = redirect('/profile-' + str(request.user.id) + '.html?error=delete_book')
        return response

@login_required
def welcome(request):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

    options = {
        'success': request.GET.get('success', None),
        'error': request.GET.get('error', None),
    }
    return render(request, 'members/welcome.html', options)


def register_eft(request):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

    if request.user.is_authenticated:
        response = redirect('/members/welcome.html?success=MemberBasicForm')
        return response
    
    if request.method == 'POST':
        form = MemberBasicForm(request.POST or None)
        if form.is_valid():
            # verify the username has not been taken
            user = User(
                username=form.cleaned_data["username"], 
                first_name=form.cleaned_data["first_name"],
                last_name=form.cleaned_data["last_name"],
                email=form.cleaned_data["email"],
            )
            user.set_password(form.cleaned_data["password"])
            user.save()
        
            referral = request.session.get('referral', '')

            member = Member.objects.create(referral=referral, user=user, pricing=settings.MEMBER_PRICING_EFT, standing=settings.MEMBER_STANDING_PAID)

            user = authenticate(request, username=form.cleaned_data["username"], password=form.cleaned_data["password"])
            if user is not None:
                login(request, user)
                response = redirect('/members/welcome.html?success=MemberBasicForm')
                return response
            else:
                response = redirect('/register-eft.html?error=MemberBasicForm')
                return response
    else:
        form = MemberBasicForm()

    options = {
        'form': form,
        'success': request.GET.get('success', None),
        'error': request.GET.get('error', None),
        'seo_title': 'Register with us, Here by inLoveLike',
        'seo_description': 'Register with us to house your EFT and begin choosing whether to resell them.',
    }
    return render(request, 'members/register-eft.html', options)



# Sources: 
# https://docs.djangoproject.com/en/3.1/topics/http/sessions/
# https://docs.djangoproject.com/en/3.1/topics/auth/default/
# https://stackoverflow.com/questions/46234627/how-does-default-token-generator-store-tokens
# https://stackoverflow.com/questions/388800/how-do-i-use-the-built-in-password-reset-change-views-with-my-own-templates
# https://stackoverflow.com/questions/32217623/django-how-do-i-get-a-queryset-of-all-users-that-are-staff
# https://docs.djangoproject.com/en/3.1/ref/models/instances/