from django import template
# import re
# import urllib
# from datetime import datetime
# import pytz
# from poems.models import Poem
from django.conf import settings
# from django.utils.http import urlencode
from urllib.parse import quote 
import json

# from django.utils.html import escape, strip_tags
# from poems.templatetags import poem_tag_extras

register = template.Library()


# @register.filter(name='times') 
# def times(number):
#     return range(0, number)

# @register.filter
# def return_item(l, i):
# 	try:
# 		return l[i]
# 	except:
# 		return None

@register.filter
def user_member_pricing_free(user):
	if user.member_user.first().pricing == settings.MEMBER_PRICING_FREE:
		return settings.MEMBER_PRICING_FREE
	else:
		return False

@register.filter
def user_member_pricing_pro(user):
	if user.member_user.first().pricing == settings.MEMBER_PRICING_PRO:
		return settings.MEMBER_PRICING_PRO
	else:
		return False

@register.filter
def user_member_pricing_unlimited(user):
	if user.member_user.first().pricing == settings.MEMBER_PRICING_UNLIMITED:
		return settings.MEMBER_PRICING_UNLIMITED
	else:
		return False

@register.filter
def user_member_pricing(user):
	return user.member_user.first().pricing

@register.filter
def user_member_standing_paid(user):
	if user.member_user.first().standing == settings.MEMBER_STANDING_PAID:
		return settings.MEMBER_STANDING_PAID
	else:
		return False

@register.filter
def user_member_standing_cancelled(user):
	if user.member_user.first().standing == settings.MEMBER_STANDING_CANCELLED:
		return settings.MEMBER_STANDING_CANCELLED
	else:
		return False

@register.filter
def user_member_standing_pending(user):
	if user.member_user.first().standing == settings.MEMBER_STANDING_PENDING:
		return settings.MEMBER_STANDING_PENDING
	else:
		return False

@register.filter
def user_member_standing(user):
	return user.member_user.first().standing

@register.filter
def user_is_approved_for_new_poems(user):
	member = user.member_user.first()
	if member.standing == settings.MEMBER_STANDING_CANCELLED:
		can_create = False
	else:
		if user.is_staff or member.pricing == settings.MEMBER_PRICING_UNLIMITED:
			can_create = True
		elif member.pricing == settings.MEMBER_PRICING_PRO:
			if len(user.poems_submitter.all()) <= settings.MEMBER_POEMS_PRO:
				can_create = True
			else: 
				# Free members can't create any new Poems
				# limited to only the one they start with
				can_create = False
		else:
			can_create = False
	return can_create

@register.filter
def user_member_free_pricing_has_poem_approved(user):
	if user.member_user.first().pricing == settings.MEMBER_PRICING_FREE:
		poems_visible = user.poems_submitter.all().filter(hidden=False)
		if poems_visible and len(poems_visible) > 0:
			return True
		else:
			return False
	else:
		return False


@register.filter
def user_pricing(user):
	return user.member_user.first().pricing

@register.filter
def user_standing(user):
	return user.member_user.first().standing

@register.filter(takes_context=True)
def profile_url(user, request):
	if user is None:
		return False

	if user.first_name or user.last_name:
		name = " ".join([user.first_name, user.last_name])
		handle = '-' + author_title(user)
	elif user.username:
		handle = '-' + author_title(user)
	else:
		handle = ''

	return host(request) + "profile" + handle + '-' + str(user.id) + '.html'


@register.filter
def author_title(user):
	if user is None:
		return False

	if user.first_name or user.last_name:
		name = " ".join([user.first_name, user.last_name])
		return name
	elif user.username:
		return user.username
	else:
		return user.email

@register.filter
def author_url(user):
	if user is None:
		return False

	if user.first_name or user.last_name:
		name_url = "+".join([user.first_name, user.last_name])
		url = '/profile-' + name_url + '-' + str(user.id) + '.html'
	elif user.username:
		url = '/profile-' + user.username + '-' + str(user.id) + '.html'
	else:
		url = '/profile-' + str(user.id) + '.html'

	return quote(url)

@register.filter
def author(user):
	if user is None:
		return False

	return '<a href="' + author_url(user) + '" data-toggle="tooltip" data-placement="left" title="Author">' + author_title(user) + '</a>'

@register.filter(takes_context=True)
def poem_item_search_default(user, setting):
	if user.is_authenticated is False:
		if setting != settings.POEM_ITEM_SEARCH_DEFAULT_CHOICE:
			return 'style="display: none;"'
		else:
			return ''

	member = user.member_user.first()

	if setting == member.ux_poem_item_search:
		return 'style="display: none;"'
	else:
		return ''

@register.filter
def home_screen_url(user):
	if user.is_authenticated:
		member = user.member_user.first()
		home_screen = member.ux_home_screen
	else:
		# Use setting
		home_screen = settings.HOME_SCREEN_DEFAULT_CHOICE

	if 'all_poems' == home_screen:
		return '/?all_poems'
	elif 'profile' == home_screen:
		return author_url(user)
	elif 'poem_create' == home_screen:
		return '/poems/create.html'
	elif 'my_poems' == home_screen:
		return '/poems/my.html'
	elif 'eft_purchases' == home_screen:
		return '/poems/eft_purchases.html'
	elif 'eft_offers' == home_screen:
		return '/poems/eft_offers.html'
	elif 'welcome' == home_screen:
		return '/members/welcome.html'
	else:
		return '/?' + home_screen

@register.filter
def member_pricing(user):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.pricing == settings.MEMBER_PRICING_FREE:
			return "Aspiring Poet"
		elif member.pricing == settings.MEMBER_PRICING_PRO:
			return "Professional Poet"
		elif member.pricing == settings.MEMBER_PRICING_UNLIMITED:
			return "Unlimited Poet"
		else:
			return member.pricing
	else: 
		return "Error"

@register.filter
def member_standing(user):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.standing == settings.MEMBER_STANDING_PENDING:
			return "Pending Payment"
		elif member.standing == settings.MEMBER_STANDING_PAID:
			if member.pricing == settings.MEMBER_PRICING_FREE:
				return "Complementry"
			else:
				return "Paid"
		elif member.standing == settings.MEMBER_STANDING_CANCELLED:
			return "Cancelled"
		else:
			return member.standing
	else: 
		return "Error"

@register.filter
def member_is_unlimited(user):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.pricing == settings.MEMBER_PRICING_UNLIMITED:
			return True
		else:
			return False
	else:
		return False

@register.filter
def member_is_pro(user):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.pricing == settings.MEMBER_PRICING_PRO:
			return True
		else:
			return False
	else:
		return False

@register.filter
def member_is_free(user):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.pricing == settings.MEMBER_PRICING_FREE:
			return True
		else:
			return False
	else:
		return False

@register.filter
def member_bio(user):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.bio:
			return member.bio
		else:
			return False
	else:
		return False

@register.filter
def member_website(user):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.website:
			return member.website
		else:
			return False
	else:
		return False

@register.filter
def member_sell_eft(user):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.ux_sell_all_poems_as_eft == 'agree':
			return True
		else:
			return False
	else:
		return False

@register.filter
def member_tips_is(user, setting):
	if user.is_authenticated:
		member = user.member_user.first()
		if member.ux_tips == setting:
			return True
		else:
			return False
	else:
		return False



# Sources:
# https://stackoverflow.com/questions/51580548/django-how-can-i-set-manually-datetimefield-value-given-date-and-time
# https://stackoverflow.com/questions/15307623/cant-compare-naive-and-aware-datetime-now-challenge-datetime-end
# https://stackoverflow.com/questions/433162/can-i-access-constants-in-settings-py-from-templates-in-django
# https://docs.djangoproject.com/en/3.1/topics/db/queries/
# https://stackoverflow.com/questions/20395691/django-get-all-records-of-related-models
# https://docs.python.org/2/library/urllib.html#urllib.quote