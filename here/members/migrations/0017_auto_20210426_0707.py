# Generated by Django 3.1.7 on 2021-04-26 07:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0016_auto_20210422_1841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalmember',
            name='pricing',
            field=models.CharField(choices=[('basic', 'Basic'), ('free', 'Free'), ('professional', 'Professional'), ('unlimited', 'Unlimited')], default='free', max_length=255),
        ),
        migrations.AlterField(
            model_name='member',
            name='pricing',
            field=models.CharField(choices=[('basic', 'Basic'), ('free', 'Free'), ('professional', 'Professional'), ('unlimited', 'Unlimited')], default='free', max_length=255),
        ),
    ]
