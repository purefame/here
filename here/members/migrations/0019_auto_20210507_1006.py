# Generated by Django 3.2.2 on 2021-05-07 10:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0018_auto_20210503_1425'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalmember',
            name='ux_tips',
            field=models.CharField(choices=[('verbose', 'Verbose'), ('some', 'Some'), ('none', 'None')], default='agree', max_length=255),
        ),
        migrations.AddField(
            model_name='member',
            name='ux_tips',
            field=models.CharField(choices=[('verbose', 'Verbose'), ('some', 'Some'), ('none', 'None')], default='agree', max_length=255),
        ),
    ]
