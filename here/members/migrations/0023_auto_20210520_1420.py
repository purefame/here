# Generated by Django 3.2.2 on 2021-05-20 14:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0022_auto_20210517_1746'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalmember',
            name='ux_home_screen',
            field=models.CharField(choices=[('welcome', 'Welcome'), ('profile', 'Profile'), ('eft_purchases', 'EFT Purchases'), ('eft_offers', 'EFT Offers')], default='welcome', max_length=255),
        ),
        migrations.AlterField(
            model_name='member',
            name='ux_home_screen',
            field=models.CharField(choices=[('welcome', 'Welcome'), ('profile', 'Profile'), ('eft_purchases', 'EFT Purchases'), ('eft_offers', 'EFT Offers')], default='welcome', max_length=255),
        ),
    ]
