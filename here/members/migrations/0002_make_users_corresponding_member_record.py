from django.db import migrations
from django.conf import settings

def make_all_users_a_member_record(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Member = apps.get_model('members', 'Member')
    User = apps.get_model('auth', 'User')
    try:
        admin = User.objects.get(pk=1)
    except:
        admin = False

    if admin:
        for user in User.objects.all():
            if user.member_user.first() is None:
                poems_visible = user.poems_submitter.all().filter(hidden=False)
                if poems_visible and len(poems_visible) > 100:
                    Member.objects.create(user=user, pricing=settings.MEMBER_PRICING_UNLIMITED)
                elif poems_visible and len(poems_visible) <= 100 and len(poems_visible) > 1:
                    Member.objects.create(user=user, pricing=settings.MEMBER_PRICING_PRO)
                else:
                    Member.objects.create(user=user, pricing=settings.MEMBER_PRICING_FREE)

class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(make_all_users_a_member_record),
    ]

# Sources:
# https://stackoverflow.com/questions/53038348/django-2-1-how-do-i-create-a-user-in-a-migration
# https://stackoverflow.com/questions/9616569/django-cannot-assign-u1-staffprofile-user-must-be-a-user-instance
# https://docs.djangoproject.com/en/3.1/topics/migrations/