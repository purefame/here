# Generated by Django 3.1.7 on 2021-04-20 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0013_auto_20210408_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalmember',
            name='source',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='member',
            name='source',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
