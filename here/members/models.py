from django.db import models
from simple_history.models import HistoricalRecords
from django.contrib.auth.models import User
from django.conf import settings
from django_resized import ResizedImageField
from .templatetags import member_tag_extras
from pages.models import Rss2
import uuid
import subprocess
import hashlib

class Member(models.Model):
    pricing = models.CharField(max_length=255, choices=settings.PRICING_CHOICES, default=settings.MEMBER_PRICING_FREE)
    user = models.ForeignKey(User, related_name="member_user", null=True, blank=True, on_delete=models.CASCADE)
    history = HistoricalRecords()
    standing = models.CharField(max_length=255, choices=settings.STANDING_CHOICES, default=settings.MEMBER_STANDING_PAID)
    ux_poem_item_search = models.CharField(max_length=255, choices=settings.POEM_ITEM_SEARCH_CHOICES, default=settings.POEM_ITEM_SEARCH_DEFAULT_CHOICE)
    ux_home_screen = models.CharField(max_length=255, choices=settings.HOME_SCREEN_CHOICES, default=settings.HOME_SCREEN_DEFAULT_CHOICE)
    ux_show_diff_on_poem_items = models.CharField(max_length=255, choices=settings.SHOW_DIFF_ON_POEM_ITEMS_CHOICES, default=settings.SHOW_DIFF_ON_POEM_ITEMS_DEFAULT_CHOICE)
    bio = models.TextField(null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    source = models.CharField(max_length=255, null=True, blank=True)
    referral = models.CharField(max_length=255, null=True, blank=True)
    ux_sell_all_poems_as_eft = models.CharField(max_length=255, choices=settings.SELL_ALL_POEMS_AS_EFT_CHOICES, default=settings.SELL_ALL_POEMS_AS_EFT_DEFAULT_CHOICE)
    guid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    ux_tips = models.CharField(max_length=255, choices=settings.TIPS_CHOICES, default=settings.TIPS_DEFAULT_CHOICE)
    transfer_key_type = models.CharField(max_length=255, choices=settings.TRANSFER_KEY_TYPE_CHOICES, default=settings.TRANSFER_KEY_TYPE_DEFAULT_CHOICE)
    contract_address = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):  
        return self.user.username + ": " + self.pricing + " as " + self.standing


    def save(self, *args, **kwargs):
        if self.guid and not self.contract_address:
            self.contract_address = self.get_contract_address(skip_save=True)

        return super(Member, self).save(*args, **kwargs)

    def get_contract_address(self, skip_save=False):
        if self.contract_address:
            return self.contract_address

        # offer_contract_address = str(hex(offer_guid_as_int))
        cmd = 'osprey_app_divide_me-inlovelike-v1 1 ' + settings.HTTPS_BASE_URL
        diff = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, executable="/bin/bash")
        diff_return = str(diff.stdout.read())
        diff.terminate()

        host_sha_hexdigest = hashlib.sha512(diff_return.encode('utf-8')).hexdigest()
                
        address_text = host_sha_hexdigest + ' Member GUID: ' + str(self.guid)
        address_digest = hashlib.sha512(address_text.encode('utf-8')).digest()
        address_digest_as_int = int.from_bytes(address_digest, 'big')
        
        address = str(hex(address_digest_as_int))

        self.contract_address = address

        if not skip_save:
            self.save()

        return address


class Book(models.Model):
    title = models.CharField(max_length=255)
    url = models.URLField()
    user = models.ForeignKey(User, related_name="book_user", on_delete=models.CASCADE)
    image = ResizedImageField(size=[286, 180], upload_to='members/books', blank=False)

    history = HistoricalRecords()


    def save(self, *args, **kwargs):
        rss = {
            'title': self.title,
            'url': member_tag_extras.author_url(self.user)
        }
        if self.id:
            rss['description'] = "Updated book on profile"
        else:
            rss['description'] = "Created a new book for a profile"
        
        try:
            Rss2.objects.create(title=rss['title'],description=rss['description'],url=rss['url'])
        except Exception as e:
            print("Failed to create RSS2 object")
            print(rss)
            print(e)

        return super(Book, self).save(*args, **kwargs)


# Sources:
# https://stackoverflow.com/questions/31130706/dropdown-in-django-model