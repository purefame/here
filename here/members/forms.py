from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from captcha.fields import ReCaptchaField
from django.conf import settings
from django.template.defaultfilters import filesizeformat
from members.models import Book
# from poems.models import Poem

class MemberPoemForm(forms.Form):
	first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	password =  forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

	poem_title = forms.CharField(widget=forms.TextInput(attrs={'placeholder': "Enter title of Poem", 'class': 'form-control'}))
	poem_body = forms.CharField(
		initial='', 
		widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 10}),
	)

	if settings.FEATURE_CAPTCHA:
		captcha = ReCaptchaField()

	fields = ['username', 'first_name', 'last_name', 'email', 'password', 'password_confirmation', 'poem_title', 'poem_body']

	def clean_username(self):
		data = self.cleaned_data['username']

		try:
			user = User.objects.get(username=data)
		except:
			user = False

		if user:
			raise ValidationError(
				_('%(username)s has already been taken. Please try another.'),
				params={'username': data},
			)

		# Always return a value to use as the new cleaned data, even if
		# this method didn't change it.
		return data

	def clean_email(self):
		data = self.cleaned_data['email']

		try:
			user = User.objects.get(email=data)
		except:
			user = False

		if user:
			raise ValidationError(
				_('%(email)s already in use. Please login with this.'),
				params={'email': data},
			)

		# Always return a value to use as the new cleaned data, even if
		# this method didn't change it.
		return data


class MemberProfessionalForm(forms.Form):
	first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	password =  forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
	if settings.FEATURE_CAPTCHA:
		captcha = ReCaptchaField()

	fields = ['first_name', 'last_name', 'username', 'password', 'email']

	def clean_username(self):
		data = self.cleaned_data['username']

		try:
			user = User.objects.get(username=data)
		except:
			user = False

		if user:
			raise ValidationError(
				_('%(username)s has already been taken. Please try another.'),
				params={'username': data},
			)

		# Always return a value to use as the new cleaned data, even if
		# this method didn't change it.
		return data

	def clean_email(self):
		data = self.cleaned_data['email']

		try:
			user = User.objects.get(email=data)
		except:
			user = False

		if user:
			raise ValidationError(
				_('%(email)s already in use. Please login with this.'),
				params={'email': data},
			)

		# Always return a value to use as the new cleaned data, even if
		# this method didn't change it.
		return data


class MemberProfileForm(forms.Form):
	ux_poem_item_search = forms.ChoiceField(label='Display of Poem Search Icons by Default', widget=forms.Select(attrs={'class': 'form-control'}), choices=settings.POEM_ITEM_SEARCH_CHOICES)
	ux_home_screen = forms.ChoiceField(label='Home Screen', widget=forms.Select(attrs={'class': 'form-control'}), choices=settings.HOME_SCREEN_CHOICES)
	ux_tips = forms.ChoiceField(label="Amount of Alert TIP's within the User Interface", widget=forms.Select(attrs={'class': 'form-control'}), choices=settings.TIPS_CHOICES)

	fields = ['ux_poem_item_search', 'ux_home_screen']


class MemberProfessionalProfileForm(forms.Form):
	bio = forms.CharField(
		required=False,
		initial='', 
		widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 10}),
	)
	website = forms.URLField(required=False, widget=forms.URLInput(attrs={'class': 'form-control'}))

	ux_show_diff_on_poem_items = forms.ChoiceField(label='Display changes on pieces of Poetry within the item details page', widget=forms.Select(attrs={'class': 'form-control'}), choices=settings.SHOW_DIFF_ON_POEM_ITEMS_CHOICES)

	ux_sell_all_poems_as_eft = forms.ChoiceField(label="Do you want to sell all your poems as EFT's and selectively exclude the ones you don't want to sell?", widget=forms.Select(attrs={'class': 'form-control'}), choices=settings.SELL_ALL_POEMS_AS_EFT_CHOICES)

	fields = ['bio', 'website', 'ux_show_diff_on_poem_items', 'ux_sell_all_poems_as_eft']


class BookForm(ModelForm):
	class Meta:
		model = Book
		fields = ['title', 'url', 'image']

		labels = {
			"title": "Title of book",
            "url": "Website address to acquire book",
            "image": "Cover image of book",
        }

		widgets = {
			'title': forms.TextInput(attrs={'placeholder': "Your title here", 'class': 'form-control'}),
			'url': forms.URLInput(attrs={'placeholder': "https://", 'class': 'form-control'}),
			'image': forms.ClearableFileInput(attrs={'class': 'form-control-file'}),
		}

	# Verify content type is an image
	# UploadedFile.content_type
	def clean_image(self):
	    i = self.cleaned_data['image']
	    content_type = i.content_type.split('/')[0]
	    if content_type in settings.BOOK_IMAGE_TYPES:
	        if int(i.size) > int(settings.BOOK_IMAGE_MAX_UPLOAD_SIZE):
	            raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') % (filesizeformat(settings.MAX_UPLOAD_SIZE), filesizeformat(content._size)))
	    else:
	        raise forms.ValidationError(_('File type is not supported'))
	    return i


class MemberBasicForm(forms.Form):
	first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	password =  forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
	if settings.FEATURE_CAPTCHA:
		captcha = ReCaptchaField()

	fields = ['first_name', 'last_name', 'username', 'password', 'email']

	def clean_username(self):
		data = self.cleaned_data['username']

		try:
			user = User.objects.get(username=data)
		except:
			user = False

		if user:
			raise ValidationError(
				_('%(username)s has already been taken. Please try another.'),
				params={'username': data},
			)

		# Always return a value to use as the new cleaned data, even if
		# this method didn't change it.
		return data

	def clean_email(self):
		data = self.cleaned_data['email']

		try:
			user = User.objects.get(email=data)
		except:
			user = False

		if user:
			raise ValidationError(
				_('%(email)s already in use. Please login with this.'),
				params={'email': data},
			)

		# Always return a value to use as the new cleaned data, even if
		# this method didn't change it.
		return data

class UserForm(forms.Form):
	first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

	fields = ['username', 'first_name', 'last_name', 'email']


# Sources: 
# https://docs.djangoproject.com/en/3.1/topics/forms/modelforms/
# https://docs.djangoproject.com/en/3.1/ref/forms/api/
# https://docs.djangoproject.com/en/3.1/ref/forms/fields/
# https://docs.djangoproject.com/en/3.1/ref/models/fields/
# https://docs.djangoproject.com/en/3.1/ref/forms/validation/
# https://stackoverflow.com/questions/48030567/how-to-customize-username-validation
# https://stackoverflow.com/questions/9324432/how-to-create-password-input-field-in-django
# https://docs.djangoproject.com/en/3.1/ref/validators/
# https://docs.djangoproject.com/en/3.1/ref/forms/api/
# https://docs.djangoproject.com/en/3.1/ref/forms/validation/
# https://docs.djangoproject.com/en/3.1/ref/forms/api/
# https://stackoverflow.com/questions/4788388/how-do-i-use-djangos-form-framework-for-select-options
# https://docs.djangoproject.com/en/3.1/topics/http/file-uploads/
# https://docs.djangoproject.com/en/3.1/ref/files/uploads/
# https://www.djangosnippets.org/snippets/1303/
# https://stackoverflow.com/questions/20986798/django-modelform-label-customization
# https://docs.djangoproject.com/en/3.1/ref/forms/fields/