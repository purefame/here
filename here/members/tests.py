from django.test import TestCase
from django.urls import reverse

class MemberViewTests(TestCase):
	def test_members_login(self):
	    # w = self.create_whatever()
	    url = reverse("login")
	    resp = self.client.get(url)

	    self.assertEqual(resp.status_code, 200)
	    # self.assertIn(resp.url, "/login.html?next=/poems/my.html")
	    # self.assertIn(w.title, resp.content)

# Sources:
# https://realpython.com/testing-in-django-part-1-best-practices-and-examples/
# https://stackoverflow.com/questions/37525075/what-does-tests-module-incorrectly-imported-mean
# https://docs.djangoproject.com/en/3.1/topics/testing/tools/
# https://docs.djangoproject.com/en/3.1/ref/urlresolvers/