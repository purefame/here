from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'members'

urlpatterns = [
    path('list.html', views.list, name='list'),
    path('welcome.html', views.welcome, name='welcome'),
    path('edit.html', views.edit, name='edit'),
    path('edit_update.html', views.edit_update, name='edit_update'),
    path('add_book.html', views.add_book, name='add_book'),
    path('book/<int:book_id>/delete.html', views.delete_book, name="delete_book"),
]

# Sources:
# https://docs.djangoproject.com/en/3.1/topics/http/urls/