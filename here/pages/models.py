from django.db import models
from simple_history.models import HistoricalRecords
from django.utils.timezone import now
import uuid
# from django.contrib.auth.models import User
# from django.conf import settings

class Rss2(models.Model):
    title = models.CharField(max_length=255)
    url = models.URLField()
    description = models.TextField(null=True, blank=True)
    created = models.DateTimeField(blank=False, default=now)
    guid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)

    def __str__(self):  
        return self.title + " - " + self.url

# Sources:
# https://stackoverflow.com/questions/31130706/dropdown-in-django-model