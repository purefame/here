from django import template
from django.conf import settings
from urllib.parse import quote 
import datetime
import re

register = template.Library()

# @register.filter(name='times') 
# def times(number):
#     return range(0, number)

@register.filter
def time_rss2(l):
	return l.strftime('%a, %d %b %Y %H:%m:%S UTC')


@register.filter
def session_referral(request):
	return request.session.get('referral', '')

@register.filter
def cad_dollars_display(str):
	if len(str) <= 0 or re.findall(r"Free", str, flags=re.IGNORECASE):
		return 'Free'

	if '$' in str:
		return str
	else:
		return '$' + str

@register.filter
def cost_as_display(cost):
	if cost:
		return '<i class="fab fa-btc"></i> ' + str(float(cost)).strip()
	else:
		return '<i class="fas fa-money-bill"></i> Free'

# Sources:
# https://stackoverflow.com/questions/51580548/django-how-can-i-set-manually-datetimefield-value-given-date-and-time
# https://stackoverflow.com/questions/15307623/cant-compare-naive-and-aware-datetime-now-challenge-datetime-end
# https://stackoverflow.com/questions/433162/can-i-access-constants-in-settings-py-from-templates-in-django
# https://docs.djangoproject.com/en/3.1/topics/db/queries/
# https://stackoverflow.com/questions/20395691/django-get-all-records-of-related-models
# https://docs.python.org/2/library/urllib.html#urllib.quote
# https://strftime.org/
# https://docs.djangoproject.com/en/3.1/ref/templates/builtins/#std:templatefilter-date