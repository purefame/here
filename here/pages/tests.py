from django.test import TestCase
from django.urls import reverse

class PageViewTests(TestCase):
	def test_sitemap(self):
	    url = reverse("sitemap")
	    resp = self.client.get('/sitemap.xml?html=true')

	    self.assertEqual(resp.status_code, 200)

	def test_sitemap_html(self):
	    resp = self.client.get('/sitemap.xml?html=true')

	    self.assertEqual(resp.status_code, 200)

	def test_robots(self):
	    resp = self.client.get('/robots.txt')

	    self.assertEqual(resp.status_code, 200)

# Sources:
# https://realpython.com/testing-in-django-part-1-best-practices-and-examples/
# https://stackoverflow.com/questions/37525075/what-does-tests-module-incorrectly-imported-mean
# https://docs.djangoproject.com/en/3.1/topics/testing/tools/
# https://docs.djangoproject.com/en/3.1/ref/urlresolvers/