
def seo_title_processor(request):
	return { 'seo_title': 'Here EFT Server with inLoveLike finding Poetry with Poems' }


def seo_description_processor(request):
	return { 'seo_description': 'Find Electronic File Tokens to distribute and collect Poetry by Poets writing everything from songs to even base64 files.' }	

# Sources:
# https://docs.djangoproject.com/en/dev/ref/templates/api/#writing-your-own-context-processors