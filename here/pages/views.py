from datetime import datetime

from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q, OuterRef
from django.utils.html import strip_tags

from members.models import Member
from pages.models import Rss2
from poems.models import EftPurchase

import re

def index(request):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))

    users_not_cancelled = Member.objects.exclude(standing=settings.MEMBER_STANDING_CANCELLED).values_list('user_id', flat=True)
    eft_purchases = EftPurchase.objects.filter(owner__in=list(users_not_cancelled))
    eft_purchases = eft_purchases.exclude(visible=False).order_by('imported_on').distinct().reverse()

    options = {
        'eft_purchases': eft_purchases,
        'seo_title': 'Here inLoveLike Server',
        'seo_description': 'The market of Electronic File Tokens',
    }
    return render(request, 'pages/index.html', options)

def catch_redirect(request, *args, **kwargs):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))
    if request.path == '/':
        return render(request, 'pages/index.html')
    else:
        return redirect('index')

def sitemap(request):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))
    html = request.GET.get('html', False)
    members_cancelled = Member.objects.exclude(standing=settings.MEMBER_STANDING_CANCELLED).values_list('pk', flat=True)
    members_not_cancelled = Member.objects.exclude(standing=settings.MEMBER_STANDING_CANCELLED).values_list('user_id', flat=True)
    staff = User.objects.filter(is_staff=True)

    options = {
        'now': datetime.now(),
    }

    if html:
        return render(request, 'sitemap.html', options)
    else:
        return render(request, 'sitemap.xml', options, content_type="text/xml; charset=utf-8")


def rss2(request):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))
    items = Rss2.objects.all().order_by('created').reverse()
    options = {
        'now': datetime.now(),
        'items': items,
    }
    return render(request, 'rss2.xml', options, content_type="text/xml; charset=utf-8")

def robots(request):
    request.session['referral'] = request.META.get('HTTP_REFERER', request.session.get('referral', ''))
    options = {
        'now': datetime.now(),
    }
    return render(request, 'robots.txt', options)
