
$( document ).ready(function() {
	// turn on tooltips
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	$('.title img').mouseenter(function() {
		$(this).next('span').addClass('selected');
	})

	$('.title img').mouseout(function() {
		$(this).next('span').removeClass('selected');
	})

	$('.nav-logo img').mouseenter(function() {
		$(this).next('span').addClass('selected');
	})

	$('.nav-logo img').mouseout(function() {
		$(this).next('span').removeClass('selected');
	})

	$('footer .logo img').mouseenter(function() {
		$(this).next('span').addClass('selected');
	})

	$('footer .logo img').mouseout(function() {
		$(this).next('span').removeClass('selected');
	})

	// Add the HTML GET variable to the url for the browsers. Spiders get raw XML.
	$('.sitemap').on('click', function(evt){ 
		var loc = $(this).prop('href') + '?html=true';
		document.location = loc;
		evt.preventDefault();
	});

	// $('form.recaptcha').each(function() {
	// 	$(this).on('submit', function(evt) {
	// 		evt.preventDefault();
	// 		var t = $(this);
	// 		grecaptcha.enterprise.ready(function() {
	// 		    grecaptcha.enterprise.execute('6LeheaIaAAAAAE1OtLK78emV-CFRWnLEbdX_gSsP', {action: 'LOGIN'}).then(function(token) {
	// 		       // $(t).submit();
	// 		       alert($(t).prop('class'));
 //                   $(t).unbind('submit').submit();

	// 		    });
	// 		});
	// 	})
	// });

	$('form').submit(function() {
		$('form :input, form input, form input[type=checkbox]').each(function() {
			$(this).prop('readonly', true);	
		});
		
		$(this).find('button[type="submit"],input[type="submit"]').prop('disabled',true);
		$(this).find('button[type="submit"],input[type="submit"]').each(function() {
			if($(this).data('loading')) {
				$(this).before('<span class="spinner-grow spinner-grow-sm text-info mr-3" role="status" aria-hidden="true"></span>')
			}
		});
	});



	$("form.eft-purchase-upload-ajax").submit(function (event) {
	    event.preventDefault();

    	var input_source = $(this).children("select[name=source]").val();
    	var input_sha512 = $(this).children("input[name=sha512]").val();
    	var input_key = $(this).children("input[name=key]").val();
		var token = $(this).children('[name=csrfmiddlewaretoken]').val();
		var destination = $(this).children('.data-destination').data('destination');

		var formData = {
			key: input_key,
			destination: destination,
		};
		$.ajax({
			type: "POST",
			url: input_source + "poems/eft-transfer-get/" + input_sha512 + '.json',
			data: formData,
			dataType: "json",
			encode: true,
		}).done(function (data_eft_get) {
			if (data_eft_get['version']) {
		    	var formData = {
					source: input_source,
					sha512: input_sha512,
					drm: data_eft_get['drm'],
					version: data_eft_get['version'],
					cost: data_eft_get['cost'],
				};

				$.ajax({
					headers: { "X-CSRFToken": token },
					type: "POST",
					url: "/poems/eft_purchase_upload_ajax",
					data: formData,
					dataType: "json",
					encode: true,
				}).done(function (data_eft_purchase_upload) {
					if (data_eft_purchase_upload['success'] == true) {
						var location = '/poems/eft_purchases.html?success=transfered';
						if (data_eft_purchase_upload['message']) {
							location += '&message=' + data_eft_purchase_upload['message'];
						} 
						console.log(location)
						document.location = location;
					} else {
						var location = '/poems/eft_purchases.html?error=import';
						if (data_eft_purchase_upload['message']) {
							location += '&message=' + data_eft_purchase_upload['message'];
						} 
						console.log(location)
						document.location = location;
					}
				});
			} else {
				var location = '/poems/eft_purchases.html?error=import_verify';
				if (data_eft_get['message']) {
					location += '&message=' + data_eft_get['message'];
				} 
				console.log(location)
				document.location = location;
			}
		});


	});
});

// Sources:
/*
https://stackoverflow.com/questions/5347357/jquery-get-selected-element-tag-name
https://api.jquery.com/on/
https://api.jquery.com/nextAll/
https://www.google.com/search?q=add+sibling+jquery&rlz=1C1CHBF_enCA943CA943&oq=add+sibling+jquery&aqs=chrome.0.0j0i22i30l8.3287j0j7&sourceid=chrome&ie=UTF-8
https://api.jquery.com/before/
https://www.w3schools.com/jquery/html_html.asp
https://learn.jquery.com/using-jquery-core/document-ready/
https://getbootstrap.com/docs/4.0/components/tooltips/
https://api.jquery.com/mouseenter/
https://stackoverflow.com/questions/11338592/how-can-i-bind-to-the-change-event-of-a-textarea-in-jquery
https://developer.mozilla.org/en-US/docs/Web/CSS/ID_selectors
https://api.jquery.com/multiple-selector/
https://stackoverflow.com/questions/8488729/how-to-count-the-number-of-lines-of-a-string-in-javascript
https://stackoverflow.com/questions/51700954/django-csrf-token-for-ajax/51701012
https://api.jquery.com/jquery.post/
https://stackoverflow.com/questions/17384218/jquery-input-event
https://stackoverflow.com/questions/35508000/stop-page-from-unloading-within-the-beforeunload-event-handler
https://stackoverflow.com/questions/15925251/trigger-an-event-when-user-navigates-away
*/