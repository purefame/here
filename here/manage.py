#!/usr/bin/env python3.8
"""Django's command-line utility for administrative tasks."""
import os
import sys

ENV = "production" if os.uname().nodename == 'purefame-plesk01' else "development"

def main():
    """Run administrative tasks."""
    from django.conf import settings
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'here.settings_' + ENV)
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    if 'test' in sys.argv:
        import logging
        logging.disable(logging.CRITICAL)
        settings.DEBUG = False
        settings.TEMPLATE_DEBUG = False
        settings.PASSWORD_HASHERS = [
            'django.contrib.auth.hashers.MD5PasswordHasher',
        ]
        settings.DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': 'here_inlovelikecom',
            }
        }
        settings.MIDDLEWARE_CLASSES = [
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
        ]

    execute_from_command_line(sys.argv)

if __name__ == '__main__':
    main()