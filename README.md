**Create a new here instance to host the EFT's minted by inLoveLike's collection of servers**

When you're done, you can import EFT purchases from any server of the inLoveLike network as well as possibly with your own Minting server if you so decide to do so. Check out https://inlovelike.com/pricing.html

The EFT Here Server is built using Django 3.2.2 and MySQL 5.7.33

*It's recommended that you open this README in another tab as you perform the tasks below.*

---

## Install the EFT Here Server

You'll start by creating a new virtual host for the instance preferably with an SSL.

1. Create an empty database and store the username and password

sudo -u postgres psql
postgres=# create database inlovelikecom_here_db;
postgres=# create user inlovelikecom_here_user with encrypted password 'inlovelikecom_here_password';
postgres=# grant all privileges on database inlovelikecom_here_db to inlovelikecom_here_user;
postgres=# create schema inlovelikecom_here_db;
postgres=# grant usage on schema inlovelikecom_here_db to public;
postgres=# grant create on schema inlovelikecom_here_db to public;

2. Copy the exaple .env file here /here/here/here/.env.example and remove the .example
3. Create a settings_production.py based on the example file the same way
4. Follow the instructions on bash_aliases for your development environment
5. Chmod the scripts and put them in your /usr/local/bin with the associated packages 
6. Run the migrations
  a. here_inlovelikecom_dir; python3.8 manage.py migrate
7. Create a superuser
  a. here_inlovelikecom_dir; python3.8 manage.py createsuperuser
8. Create a member record for the super user via the command prompt
  a. here_inlovelikecom_shell

from members.models import Member
from django.contrib.auth.models import User
Member.objects.create(user=User.objects.first())

9. Update the /templates/theme.html file with your own look and feel
10. Email nate@purefame.com with your server host and get added to the network
---